# Projektarbeit SWT1/DB2

## Werkzeuge

### Git

* [Git](https://git-scm.com)
* [Git-GUI-List](https://git-scm.com/downloads/guis)

### Repository einrichten

Ein auf dem Server bestehendes Repository lokal einrichten (am beispiel GitHub Desktop):

* Menu: File->Clone Repository
* Tab "URL"
* Repository-URL: https://gitlab.com/RageGroup/VapeMode.git
* Local path, z.B: P:\Projekte\Projektname

### Änderungen sichern

Das Programm listet die Änderungen auf.

* unten links im Hauptfenster eine Beschreibung der Änderung eingeben, Button "Commit to master" drücken
* unterhalb der Menüleiste befinden sich drei Button-Bereiche "Current repository", "Current branch" und ein dritter ganz rechts. Dort steht nach dem commit "Push origin" - damit wird das commit an den Server übermittelt.

Die Git-GUIs sind in den meisten Fällen nur dazu da, Änderungen zu verwalten.
Änderungen vornehmen muss man mit z.B. Visual Studio, SSMS, MS Word, Excel.

## Grundsätze

1. Es wird die Zeit und Arbeitseinteilung stets eingehalten, sollte dies nicht möglich sein, werden die Mitglieder des Teams darüber in Kenntnis gesetzt. Es wird daraufhin gemeinsam eine Lösung erarbeitet.
2. Die Zusammenarbeit und der Erfolg des Teams beruht auf Respekt und Kommunikation zwischen den Mitgliedern des Projektes.
3. Zur Sicherung der wichtigen Daten (Code, Dokumentation und Entwürfe) verwenden wir ein cloudbasiertes System. Uploads geschehen kontinuierlich in Abständen von maximal 3 Tagen beim Arbeiten. Jedes Mitglied ist auch überwachendes Organ.
4. Es findet wöchentlich eine Besprechung statt. Diese findet bei einem Treffen in der Hochschule oder in eine digitale Sitzung (Teamspeak, Discord oder Skype) statt.
5. Die Arbeit der Mitglieder wird stets dokumentiert und in ein „Logbuch“ eingetragen.
6. Wir stehen stets im Kontakt mit dem Kunden um sein Produkt optimal ausarbeiten und anschließend ausliefern zu können.
7. Es herrschen festgelegte Rollen im Team, sollte es Fragen bzw. etwas geben, wird sich an das entsprechende Teammitglied gewandt.
8. Die Ausarbeitung und Einhaltung der Pläne ist ein zentraler Punkt und essentiell für den Erfolg für den Kunden und das Produkt.
9. Programmcode muss kontinuierlich beim Entwickeln getestet und vor Abgabe an den Kunden von jedem Mitglied validiert werden.
10. Bei wichtigen Entscheidungen ist immer eine Abstimmungen einzuleiten, diese ist in unserer WhatsApp Gruppe anzumelden und erfolgt innerhalb von 3 Tagen.

## Dampfer-Shop "VapeMode"

### Selbst-Beschreibung

Wir sind VapeMode - eine kleine Kette, die E-Zigaretten, Liquids und Zubehör verkauft. Momentan gibt es uns an 6 verschiedenen Standorten und wir haben insgesamt 30 Mitarbeiter.
Aufgrund der steigenden Zahl an Konkurrenten wollen wir potenziellen Kunden größere Anreize bieten unsere Fachgeschäfte zu besuchen und diese dann als Stammkunden zu gewinnen.
Um dieses Ziel zu realisieren benötigen wir eine (neue) Datenbank. Zum einen sollen Geschäftsprozesse besser dokumentiert und ausgewertet werden können und zum anderen soll das Sortiment ausgeweitet werden und ein Bonuspunktesystem zur Gewinnung von Stammkunden integriert werden.
Wir sind der Meinung, dass glückliche und motivierte Mitarbeiter besser beraten und mehr verkaufen, weshalb wir in Zukunft Prämien, welche von der Datenbank auf Grundlage von Umsatzzahlen pro Mitarbeiter berechnet werden.
Ein weiteres Ziel ist es alle beliebten Produkte im Sortiment stets vorrätig zu haben, soll auch durch die Datenbank umgesetzt werden, welche berechnet, wie lange ein Produkt voraussichtlich noch vorrätig ist.
Eine der wichtigsten Funktionen, die die Datenbank haben sollen ist die Umsetzung des Bonuspunktesystems. Der Kunde soll die Möglichkeit haben bei jedem Einkauf in einer unserer Filialen prozentual zum Warenwert Punkte zu sammeln, welche entweder gegen Prämien oder Rabattgutscheine eingetauscht werden sollen.

## Das Projekt einer anderen Gruppe - "Myrtana Inc."

### Fragen

#### Fragen am 2018-06-13
* Werden alle Produkte von euch selber hergestellt?
  - Ja
* Kann man Produkte kaufen, die man nicht selber in Auftrag gegeben hat?
  - Wie meinst du das genau ? Im sinne von , wir haben ein bestand und genau diesen vertreiben wir und oder stellen es Herr
* Kann man alle Produkte Mieten, die man auch kaufen kann?
  - Nein nicht alle, Sonderanfertigungen sind davon ausgenommen
* Gibt es eine maximale/ minimale Teilnehmerzahl bei Kursen?
  - Nein keine Begrenzung aber alle 10 + kommt ein Trainer oben drauf also bei 15 Personen kümmern sich 2 um die Leute
* Kümmert sich der gleiche Mitarbeiter um Verkauf und Vermietung?
  - (keine Antwort)
* Sind die Preise von Kursen pro Person oder pro Kurs?
  - Ja pro Kurs
* Wie berechnet sich die Miete und Kaution der Produkte
  - Mieten 10% des Wertes und Kaution 3x Monatsmiete
* Welche Daten vom Kunden sind notwendig und welche optional?
  - Firmenname Anschrift firmensteuernummer firmeninhaben/Besteller
  - Seit wann Kunde?, 5 mal bestellt?
  - Beschwerden?
  - Optional
  - Häufig genutzte Dienste
  - Film oder Theater
* Kann man pro Vorgang mehr als einen Artikel bestellen?
  - Ja
* Falls ja: sind die Kurse und Waren  bzw Vermietung und Kauf in einer Bestellung abzuarbeiten?
  - Ja

#### Fragen am 2018-07-02
* eERM weitergegeben:
  - Mitarbeiter fehlt noch Informationen , Adresse mitarbeitet und Kunde(Adresse auslagern ) Funktion , Gehalt Mitarbeiter Einstellungs datum und welche Funktion die sind
* Was gibt es für Mitarbeiter, welche Funktionen haben sie und wie unterscheiden sie sich? Kann ein MA zugleich mehrere Funktionen haben?
  - Wie im Modell eschrieben , uns nein nur eine funktion
* Colin: können Mitarbeiter auch Kunden sein?
* Colin: Zu euren Produkten: Es gibt ja die 4 Kategorien: Waffen, Pferderüstungen, Möbel, Rüstungen. Gibt es Attribute, die sich bei diesen Kategorien unterscheiden?
* Colin: Da ihr eure Produkte ja auch vermietet ändert sich ja ständig der Zustand. Soll dieser Zustand auch in der DB gespeichert sein? Wenn ja welche Zustände gibt es? Wer legt ihn fest? Welche Konsequenzen hat der ZUstand auf Mietpreis und Kaution?
* Colin: Ist es vlt so, wie wenn eine Firma sich ne Maschine kauft und die dann z.B. nach 5 Jahren abgeschrieben ist ?
* Colin: am einfachsten für die Implementierung wäre es, wenn es nur 3 Zustände gäbe: so  in etwa wie "sehr gut", "aktzeptabel" und "mangelhaft" und bei mangelhaft bestünde dann der Reparaturbedarf
* Colin: Wie berechnen sich der Preis für die Gruppenkurse? Pro Person, pro Trainer oder pro Trainer + Zuschlag pro Person?
* Colin: Kann ein Produkt bestellt und ein Kurs in der gleichen Bestellung gebucht werden?
* Colin: Leitet ein Trainer verschiedene Kurse oder nur einen?
* Colin: Mitarbeiterverwaltung: Wer vervaltet die Mitartbeiter? gibt es irgendeine Art von Hierachie mit vorgesetzten usw.?
* agiert ihr nur in DE?

### Dokumentation

Werkzeuge:
* Microsoft Word:                           Text Document
* Freeplane:                                Mindmap (Context/Function Tree)
* PlantUML:                                 Requirements Diagram, Use Case Diagram, Component Diagram
* yEd:                                      BPMN/Activity Diagram, Enhanced Entity-Relationship Model
* Microsoft Excel:                          Relation Model
* Oracle SQL Developer Data Modeler:        Relation Model

Die Dokumentation liegt im Unterordner "Myrtana/doc".

Hier eine Beschreibung der Unterordner darin in Reihenfolge der Nutzung:

Für "SWT1":
1. Mindmap (Context): Die Sammlung der Anforderungsschnipsel und der Kontextgrenzen
2. Requirements Diagram: Die nicht-funktionalen Anforderungen
3. Use Case Diagram: Die Beschreibung der Anwendungsfälle (grob/fein)
4. Component Diagram: Die Programmkomponenten (grob/fein)
5. Mindmap (Function Tree): Die hierarchische Einordnung der Funktionen
6. BPMN/Activity Diagram: Aufbau der Prozeduren, Funktionen und Trigger der Datenbank

Die gesammelten Texte und Grafiken werden in "VapeMode.docx" vereinigt.
Die Abgabe soll dann unter dem Namen "VapeMode.pdf" erfolgen.
Dieses Dokument soll folgendes enthalten (siehe SWT-Projektabgabe):
* Deckblatt - das Entwicklerteam stellt sich vor
* Zielbestimmung                                                                --> 1. MM (C); 2. RD
* Produkteinsatz                                                                --> 3. UCD
* Produktübersicht                                                              --> 4. CD
* Produktfunktionen                                                             --> 5. MM (FT); 6. BPNM/AD
* Produktdaten (nur allgemeine Beschreibung der benötigten Daten)
* Technische Produktumgebung (Software, Hardware, Orgware)
* Anhang - Überblick über die geleistete Arbeit (Wochenpläne mit Aufgaben und Verantwortlichkeiten sowie Ergebnissen)

Für "DB2":
1. Enhanced Entity-Relationship Model: Grober Entwurf der Datenbank (als Grafik in einer pdf-Datei)
2. Relation Model: Genauer Aufbau der Datenbank (als pdf-Datei)
3. Erstellungsskript (als sql-Datei)

Diese Teile sollen in ein Archiv verpackt sein.

### Umsetzung

Werkzeuge:
* Microsoft SQL Server Management Studio:   Bearbeitung und Test der SQL-Skripte
* Microsoft SQL Server:                     Eingesetztes Datenbanksystem

## Fragen an Frau Rossak

* Projektpräsentation: Form
  - Folien möglich, aber nicht nötig
* Nicht-funktionale Anforderungen
  - falls vom Kunden nichts dazu kam, dann das zumindest in der Doku festhalten
  - Geplante Tests zählen dazu
