<map version="freeplane 1.6.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Myrtana Inc." FOLDED="false" ID="ID_102360432" CREATED="1527596392734" MODIFIED="1527941589788" STYLE="oval">
<font NAME="Fira Code" SIZE="18"/>
<hook NAME="MapStyle" layout="OUTLINE">
    <properties edgeColorConfiguration="#808080ff,#ff0000ff,#0000ffff,#00ff00ff,#ff00ffff,#00ffffff,#7c0000ff,#00007cff,#007c00ff,#7c007cff,#007c7cff,#7c7c00ff" show_icon_for_attributes="true" show_notes_in_map="true" show_note_icons="true" fit_to_viewport="false"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node" STYLE="oval" UNIFORM_SHAPE="true" VGAP_QUANTITY="24.0 pt">
<font SIZE="24"/>
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="default" ICON_SIZE="12.0 pt" COLOR="#000000" STYLE="fork">
<font NAME="Fira Code" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details">
<font NAME="Fira Code"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.attributes">
<font NAME="Fira Code" SIZE="9"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.note" COLOR="#000000" BACKGROUND_COLOR="#ffffff" TEXT_ALIGN="LEFT">
<font NAME="Fira Code"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
<font NAME="Fira Code"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Fira Code" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Fira Code" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Fira Code" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
<font NAME="Fira Code"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000" STYLE="oval" SHAPE_HORIZONTAL_MARGIN="10.0 pt" SHAPE_VERTICAL_MARGIN="10.0 pt">
<font NAME="Fira Code" SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font NAME="Fira Code" SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font NAME="Fira Code" SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font NAME="Fira Code" SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font NAME="Fira Code" SIZE="10"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,5">
<font NAME="Fira Code"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,6">
<font NAME="Fira Code"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,7">
<font NAME="Fira Code"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,8">
<font NAME="Fira Code"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,9">
<font NAME="Fira Code"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,10">
<font NAME="Fira Code"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,11">
<font NAME="Fira Code"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook NAME="AutomaticEdgeColor" COUNTER="9" RULE="ON_BRANCH_CREATION"/>
<richcontent TYPE="NOTE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &quot;Zwischen Feuer und Eis&quot;
    </p>
  </body>
</html>
</richcontent>
<node TEXT="Interessenten" POSITION="right" ID="ID_23833940" CREATED="1527600129320" MODIFIED="1527600137945">
<edge COLOR="#007c00"/>
<font NAME="Fira Code"/>
<node TEXT="Kunden" ID="ID_1980810195" CREATED="1527596522367" MODIFIED="1527941931312"><richcontent TYPE="NOTE">

<html>
  <head>
    
  </head>
  <body>
    <ul>
      <li>
        nur Gesch&#228;ftskunden (keine Privatpersonen)
      </li>
    </ul>
  </body>
</html>

</richcontent>
<font NAME="Fira Code"/>
</node>
</node>
<node TEXT="Produktkategorien" POSITION="left" ID="ID_980530321" CREATED="1527599090815" MODIFIED="1527941641259">
<edge COLOR="#ff00ff"/>
<richcontent TYPE="NOTE">

<html>
  <head>
    
  </head>
  <body>
    <ul>
      <li>
        Alles aus eigener Herstellung
      </li>
    </ul>
  </body>
</html>

</richcontent>
<font NAME="Fira Code"/>
<node TEXT="Waffen" ID="ID_1936246204" CREATED="1527599476884" MODIFIED="1527599918220">
<font NAME="Fira Code"/>
</node>
<node TEXT="R&#xfc;stungen" ID="ID_1512725042" CREATED="1527599483356" MODIFIED="1527599918221">
<font NAME="Fira Code"/>
</node>
<node TEXT="Pferder&#xfc;stungen" ID="ID_186985418" CREATED="1527599713600" MODIFIED="1527599918221">
<font NAME="Fira Code"/>
</node>
<node TEXT="M&#xf6;bel" ID="ID_169296682" CREATED="1527599768219" MODIFIED="1527599918222">
<font NAME="Fira Code"/>
</node>
</node>
<node TEXT="Leistungen" POSITION="right" ID="ID_1319841300" CREATED="1527599632997" MODIFIED="1527599918223">
<edge COLOR="#7c0000"/>
<font NAME="Fira Code"/>
<node TEXT="Herstellung" ID="ID_1241457755" CREATED="1527599678166" MODIFIED="1527599918223">
<font NAME="Fira Code"/>
</node>
<node TEXT="Verleih" ID="ID_1200131765" CREATED="1527599652453" MODIFIED="1527599918224">
<font NAME="Fira Code"/>
</node>
<node TEXT="Reparaturen" ID="ID_547720141" CREATED="1527599567524" MODIFIED="1527599918225">
<font NAME="Fira Code"/>
</node>
<node TEXT="Gruppenkurse" ID="ID_1777382265" CREATED="1527599047195" MODIFIED="1527599918226"><richcontent TYPE="NOTE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Kurse nach Vereinbarung
    </p>
  </body>
</html>
</richcontent>
<font NAME="Fira Code"/>
<node TEXT="Sprechtraining" ID="ID_1045264186" CREATED="1527599734691" MODIFIED="1527599918226">
<font NAME="Fira Code"/>
</node>
<node TEXT="Ausr&#xfc;stungsschulung" ID="ID_1149116493" CREATED="1527599742164" MODIFIED="1527599918227">
<font NAME="Fira Code"/>
</node>
</node>
</node>
<node TEXT="Mitarbeiter" POSITION="right" ID="ID_1592669134" CREATED="1527599136631" MODIFIED="1527599918222">
<edge COLOR="#00ffff"/>
<font NAME="Fira Code"/>
<node TEXT="Trainer" ID="ID_1491731127" CREATED="1527599142470" MODIFIED="1527599918222"><richcontent TYPE="NOTE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Je Kurs ein Trainer
    </p>
  </body>
</html>
</richcontent>
<font NAME="Fira Code"/>
</node>
</node>
<node TEXT="Auswertungen" POSITION="right" ID="ID_375466646" CREATED="1527599805761" MODIFIED="1527599918228">
<edge COLOR="#00007c"/>
<font NAME="Fira Code"/>
<node TEXT="Auslastung je Produkt" ID="ID_1569064327" CREATED="1527599841898" MODIFIED="1527599918228">
<font NAME="Fira Code"/>
</node>
<node TEXT="Auflistung der Kunden" ID="ID_1517483995" CREATED="1527599908062" MODIFIED="1527599918205">
<font NAME="Fira Code"/>
</node>
<node TEXT="Anregungen durch Kunden" ID="ID_1163239335" CREATED="1527599946202" MODIFIED="1527599957292">
<font NAME="Fira Code"/>
</node>
</node>
</node>
</map>
