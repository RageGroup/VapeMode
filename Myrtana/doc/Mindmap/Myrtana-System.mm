<map version="freeplane 1.6.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Myrtana Inc." FOLDED="false" ID="ID_102360432" CREATED="1527596392734" MODIFIED="1530480518266" STYLE="oval">
<font NAME="Verdana" SIZE="18"/>
<hook NAME="MapStyle" layout="OUTLINE">
    <properties edgeColorConfiguration="#808080ff,#ff0000ff,#0000ffff,#00ff00ff,#ff00ffff,#00ffffff,#7c0000ff,#00007cff,#007c00ff,#7c007cff,#007c7cff,#7c7c00ff" show_icon_for_attributes="true" show_notes_in_map="true" show_note_icons="true" fit_to_viewport="false"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node" STYLE="oval" UNIFORM_SHAPE="true" VGAP_QUANTITY="24.0 pt">
<font NAME="Verdana" SIZE="24"/>
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right" STYLE="bubble">
<font NAME="Verdana"/>
<stylenode LOCALIZED_TEXT="default" ICON_SIZE="12.0 pt" COLOR="#000000" STYLE="fork">
<font NAME="Verdana" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details">
<font NAME="Verdana"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.attributes">
<font NAME="Verdana" SIZE="9"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.note" COLOR="#000000" BACKGROUND_COLOR="#ffffff" TEXT_ALIGN="LEFT">
<font NAME="Verdana"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
<font NAME="Verdana"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right" STYLE="bubble">
<font NAME="Verdana"/>
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Verdana" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Verdana" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Verdana" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
<font NAME="Verdana"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right" STYLE="bubble">
<font NAME="Verdana"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000" STYLE="oval" SHAPE_HORIZONTAL_MARGIN="10.0 pt" SHAPE_VERTICAL_MARGIN="10.0 pt">
<font NAME="Verdana" SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font NAME="Verdana" SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font NAME="Verdana" SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font NAME="Verdana" SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font NAME="Verdana" SIZE="10"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,5">
<font NAME="Verdana"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,6">
<font NAME="Verdana"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,7">
<font NAME="Verdana"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,8">
<font NAME="Verdana"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,9">
<font NAME="Verdana"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,10">
<font NAME="Verdana"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,11">
<font NAME="Verdana"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook NAME="AutomaticEdgeColor" COUNTER="14" RULE="ON_BRANCH_CREATION"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
<node TEXT="Umgebung" POSITION="right" ID="ID_23833940" CREATED="1527600129320" MODIFIED="1530480518279">
<edge COLOR="#007c00"/>
<font NAME="Verdana" BOLD="true"/>
<node TEXT="Einkauf (bleibt au&#xdf;en vor)" ID="ID_187094045" CREATED="1528058919030" MODIFIED="1530480518289">
<font NAME="Verdana"/>
<node TEXT="Werkzeuge" ID="ID_232002423" CREATED="1528058965425" MODIFIED="1530480518290">
<font NAME="Verdana"/>
</node>
<node TEXT="Material" ID="ID_950251724" CREATED="1528058970037" MODIFIED="1530480518291">
<font NAME="Verdana"/>
</node>
</node>
<node TEXT="Rechnungswesen (bleibt au&#xdf;en vor)" ID="ID_154603080" CREATED="1531061690918" MODIFIED="1531061722488">
<font NAME="Verdana"/>
</node>
<node TEXT="Privatkunden (gibt es nicht)" ID="ID_1934136478" CREATED="1528059087861" MODIFIED="1530480518292">
<font NAME="Verdana"/>
</node>
<node TEXT="Kontext" ID="ID_1980810195" CREATED="1527596522367" MODIFIED="1530480518293">
<font NAME="Verdana" BOLD="true"/>
<cloud COLOR="#99ffff" SHAPE="ROUND_RECT"/>
<node TEXT="Pflege der Leistungen" ID="ID_1626807752" CREATED="1528059512306" MODIFIED="1530480518295">
<font NAME="Verdana"/>
</node>
<node TEXT="Auswertung des Kunden-Feedbacks" ID="ID_1943314457" CREATED="1528060096036" MODIFIED="1530480518295">
<font NAME="Verdana" ITALIC="true"/>
</node>
<node TEXT="Abarbeitung der Bestellungen" ID="ID_1939354611" CREATED="1531061459206" MODIFIED="1531061547826">
<font NAME="Verdana"/>
<node TEXT="Planung von Herstellung/Reparatur" ID="ID_123922833" CREATED="1531061594214" MODIFIED="1531061624640">
<font NAME="Verdana"/>
</node>
<node TEXT="Planung der Gruppenkurse" ID="ID_1787416537" CREATED="1531061492630" MODIFIED="1531061547822">
<font NAME="Verdana"/>
</node>
<node TEXT="Reparaturkosten (vom Bearbeiter vorzugeben)" ID="ID_1391773277" CREATED="1531065841395" MODIFIED="1531384357415">
<font NAME="Verdana"/>
</node>
</node>
<node TEXT="System" ID="ID_1029070793" CREATED="1528057863249" MODIFIED="1530480518306">
<cloud COLOR="#ccff99" SHAPE="ROUND_RECT"/>
<font NAME="Verdana" BOLD="true"/>
<node TEXT="Produkte f&#xfc;r Herstellung (Muss)" ID="ID_1193896482" CREATED="1528060201165" MODIFIED="1531061314485">
<font NAME="Verdana"/>
<node TEXT="Waffen" ID="ID_544332035" CREATED="1528060224129" MODIFIED="1530480518308">
<font NAME="Verdana"/>
</node>
<node TEXT="R&#xfc;stungen" ID="ID_759304335" CREATED="1528060228030" MODIFIED="1530480518308">
<font NAME="Verdana"/>
</node>
<node TEXT="Pferder&#xfc;stungen" ID="ID_321573159" CREATED="1528060231110" MODIFIED="1530480518308">
<font NAME="Verdana"/>
</node>
</node>
<node TEXT="Leistungen (Muss)" ID="ID_241670580" CREATED="1528059046509" MODIFIED="1530480518309">
<font NAME="Verdana"/>
<node TEXT="Produkte" ID="ID_561860856" CREATED="1531384229345" MODIFIED="1531384243607">
<font NAME="Verdana"/>
<node TEXT="Herstellung" ID="ID_1381588329" CREATED="1528058041170" MODIFIED="1530480518309">
<font NAME="Verdana"/>
</node>
<node TEXT="Reparaturen" ID="ID_363480674" CREATED="1528058052673" MODIFIED="1530480518309">
<font NAME="Verdana"/>
</node>
</node>
<node TEXT="Gruppenkurse" ID="ID_1302873870" CREATED="1528058060298" MODIFIED="1530480518310">
<font NAME="Verdana"/>
<node TEXT="Ausr&#xfc;stungsschulung" ID="ID_1067558749" CREATED="1528059017874" MODIFIED="1530480518310">
<font NAME="Verdana"/>
</node>
<node TEXT="Sprechtraining" ID="ID_1078830268" CREATED="1528059024798" MODIFIED="1530480518310">
<font NAME="Verdana"/>
</node>
</node>
</node>
<node TEXT="Interessenten" ID="ID_867447209" CREATED="1528059081550" MODIFIED="1530480518311">
<font NAME="Verdana"/>
<node TEXT="Kunden: Gesch&#xe4;ftskunden (Muss)" ID="ID_1393709634" CREATED="1528059087861" MODIFIED="1530480518311">
<font NAME="Verdana"/>
<node TEXT="Kunden-Feedback (Wunsch)" ID="ID_987006613" CREATED="1528058860422" MODIFIED="1530480518312">
<font NAME="Verdana" ITALIC="true"/>
</node>
</node>
</node>
<node TEXT="Personalverwaltung (Muss)" ID="ID_1998505911" CREATED="1531061361631" MODIFIED="1531384264496">
<font NAME="Verdana"/>
</node>
</node>
</node>
</node>
</node>
</map>
