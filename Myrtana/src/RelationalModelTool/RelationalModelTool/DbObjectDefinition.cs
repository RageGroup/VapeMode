﻿using System.Collections.Generic;
using System.Linq;
using System.IO;

namespace RelationalModelTool
{
  public abstract class DbObjectDefinition
  {
    public string ObjectType { get; }
    public string ObjectName { get; protected set; }

    protected DbObjectDefinition(string objectType, string objectName)
    {
      this.ObjectName = objectName;
      this.ObjectType = objectType;
    }

    protected DbObjectDefinition(string objectType):
      this(objectType, null)
    {
    }

    public string GetLongObjectType()
    {
      switch (this.ObjectType)
      {
        case "P":
          return "procedure";
        case "FN":
          return "function";
        case "TR":
          return "trigger";
        case "U":
          return "table";
      }
      return null;
    }

    public string GetDropObjectCmd()
    {
      return $"if object_id('{this.ObjectName}', '{this.ObjectType}') is not null drop {this.GetLongObjectType()} [{this.ObjectName}]";
    }

    public abstract IReadOnlyList<string> GetSqlCommands();

    public override string ToString()
    {
      return $@"Database Object ""{this.ObjectName}""";
    }
  }
}
