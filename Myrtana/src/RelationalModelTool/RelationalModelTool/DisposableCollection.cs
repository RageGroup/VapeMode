﻿using System;
using System.Data;
using System.Data.Common;
using System.Data.OleDb;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using Excel = Microsoft.Office.Interop.Excel;

namespace RelationalModelTool
{
  public class DisposableCollection<T>: Collection<T>, IDisposable
    where T: IDisposable
  {
    protected override void ClearItems()
    {
      foreach (var item in this)
      {
        item.Dispose();
      }
      base.ClearItems();
    }

    protected override void RemoveItem(int index)
    {
      this[index].Dispose();
      base.RemoveItem(index);
    }

    #region Methods
    /// <summary>
    /// Explicitly dispose managed and unmanaged resources if necessary.
    /// </summary>
    public void Dispose()
    {
      this.Dispose(true);
      GC.SuppressFinalize(this);
    }

    /// <summary>
    /// Dispose unmanaged resources. Will be called at most once when explicitly disposing or when finalizing.
    /// </summary>
    protected virtual void DisposeUnmanaged()
    {
      foreach (var item in this)
      {
        item.Dispose();
      }
    }

    /// <summary>
    /// Dispose managed resources. Will be called at most once when explicitly disposing but not when finalizing.
    /// </summary>
    protected virtual void DisposeManaged()
    {
    }

    /// <summary>
    /// Dispose managed and unmanaged resources if necessary.
    /// </summary>
    /// <param name="disposing">Explicit disposal requested? False when finalizing.</param>
    protected virtual void Dispose(bool disposing)
    {
      if (!this._disposed)
      {
        if (disposing)
        {
          this.DisposeManaged();
        }
        this.DisposeUnmanaged();
        this._disposed = true;
        var disposed = this.Disposed;
        if (disposed != null)
        {
          disposed(this, EventArgs.Empty);
        }
      }
    }
    #endregion Methods

    #region Events
    /// <summary>
    /// Event that is fired, when the object has been disposed.
    /// </summary>
    public virtual event EventHandler Disposed;
    #endregion Events

    #region Properties
    /// <summary>
    /// Has the object been disposed?
    /// </summary>
    public virtual bool IsDisposed
    {
      get { return this._disposed; }
    }
    #endregion Properties

    #region Fields
    /// <summary>
    /// Has the object been disposed?
    /// </summary>
    private bool _disposed;
    #endregion Fields
  }
}
