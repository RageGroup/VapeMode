﻿using System.Runtime.InteropServices;
using Excel = Microsoft.Office.Interop.Excel;

namespace RelationalModelTool
{
  public class ExcelCells: DisposableBase
  {
    protected Excel.Range _usedRange;

    internal ExcelCells(Excel.Worksheet worksheet)
    {
      this._usedRange = worksheet.UsedRange;
      //var range = this._worksheet.UsedRange;
      //var rowCount = range.Rows.Count;
      //var columnCount = range.Columns.Count;
      //var cells = range.Cells[1, 1] as Excel.Range;
      //var value = cells.Value2 as string;
    }

    protected override void DisposeUnmanaged()
    {
      Marshal.ReleaseComObject(this._usedRange);
      base.DisposeUnmanaged();
    }

    public object this[int row, int column]
    {
      get => (this._usedRange.Cells[row, column] as Excel.Range).Value2;
    }

    public int RowCount => this._usedRange.Rows.Count;

    public int ColumnCount => this._usedRange.Columns.Count;
  }
}
