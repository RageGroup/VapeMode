﻿using System.Collections.Generic;
using System.Text;

namespace RelationalModelTool
{
  public class TableCheckDefinition: TableConstraintDefinition
  {
    public string CheckCode { get; }

    public TableCheckDefinition(
      string columnName,
      string checkCode) :
      base("CK_" + columnName)
    {
      this.CheckCode = checkCode.Replace("\r\n", " ").Replace("\r", " ").Replace("\n", " ");
    }

    public override string GetConstraintCommand()
    {
      return $@"constraint {this.ConstraintName} check ({this.CheckCode})";
    }

    public override string ToString()
    {
      return $@"Foreign Key ""{this.ConstraintName}""";
    }
  }
}
