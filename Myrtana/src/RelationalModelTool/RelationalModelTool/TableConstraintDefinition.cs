﻿namespace RelationalModelTool
{
  public abstract class TableConstraintDefinition
  {
    public string ConstraintName { get; }

    protected TableConstraintDefinition(
      string constraintName)
    {
      this.ConstraintName = constraintName;
    }

    public abstract string GetConstraintCommand();

    public override string ToString()
    {
      return $@"Constraint ""{this.ConstraintName}""";
    }
  }
}
