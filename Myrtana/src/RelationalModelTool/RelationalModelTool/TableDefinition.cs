﻿using System.Collections.Generic;
using System.Linq;
using System.IO;

namespace RelationalModelTool
{
  public class TableDefinition: DbObjectDefinition
  {
    public string TableName => this.ObjectName;
    public string Description { get; }
    public IReadOnlyList<TableColumnDefinition> Columns { get; }
    public TablePrimaryKeyDefinition PrimaryKey { get; }
    public IReadOnlyList<TableForeignKeyDefinition> ForeignKeys { get; }
    public IReadOnlyList<TableCheckDefinition> Checks { get; }

    public IReadOnlyList<TableColumnDefinition> GetNonPrimaryKeyColumns()
    {
      if (this.PrimaryKey == null)
      {
        return this.Columns;
      }
      var cols = new List<TableColumnDefinition>();
      foreach (var col in this.Columns)
      {
        if (!this.PrimaryKey.Columns.Contains(col))
        {
          cols.Add(col);
        }
      }
      return cols;
    }

    public override IReadOnlyList<string> GetSqlCommands()
    {
      // TODO
      return null;
    }

    public TableDefinition(ExcelCells cells):
      base("U")
    {
      this.ObjectName = cells[1, 1] as string;
      this.Description = cells[1, 2] as string;
      var columns = new List<TableColumnDefinition>();
      var primaryKeyColumns = new List<TableColumnDefinition>();
      var foreignKeys = new Dictionary<string, TableForeignKeyDefinition>();
      var checks = new List<TableCheckDefinition>();
      this.Checks = checks;
      var rowCount = cells.RowCount;
      for (var r = 4; r <= rowCount; r++)
      {
        var column = new TableColumnDefinition(
          columnId: r - 3,
          columnName: cells[r, 1] as string,
          description: cells[r, 2] as string,
          dataType: cells[r, 3] as string,
          optional: (cells[r, 4] as string == "not null" ? false : true),
          defaultValue: cells[r, 5] as string,
          primaryKey: (cells[r, 6] as string == "Y" ? true : false),
          foreignKeyTable: cells[r, 7] as string,
          foreignKeyColumn: cells[r, 8] as string,
          checks: cells[r, 9] as string);
        columns.Add(column);

        if (column.PrimaryKey)
        {
          primaryKeyColumns.Add(column);
        }
        if (!(string.IsNullOrWhiteSpace(column.ForeignKeyTable) ||
              string.IsNullOrWhiteSpace(column.ForeignKeyColumn)))
        {
          if (!foreignKeys.ContainsKey(column.ForeignKeyTable))
          {
            foreignKeys[column.ForeignKeyTable] = new TableForeignKeyDefinition(this.TableName, column.ForeignKeyTable);
          }
          foreignKeys[column.ForeignKeyTable].AddColumn(column.ColumnName, column.ForeignKeyColumn);
        }
        if (!string.IsNullOrWhiteSpace(column.Checks))
        {
          checks.Add(new TableCheckDefinition(column.ColumnName, column.Checks));
        }
      }
      if (primaryKeyColumns.Count > 0)
      {
        this.PrimaryKey = new TablePrimaryKeyDefinition(this.TableName, primaryKeyColumns);
      }
      this.ForeignKeys = new List<TableForeignKeyDefinition>(foreignKeys.Values);
      this.Columns = columns;
    }

    public void WriteCreateTableCommands(StreamWriter streamWriter)
    {
      streamWriter.WriteLine($@"if object_id('{this.TableName}', 'U') is not null drop table [{this.TableName}]");
      streamWriter.WriteLine(@"go");
      streamWriter.WriteLine();
      streamWriter.WriteLine($@"create table [{this.TableName}](");
      var maxColNameLen = this.Columns.Max(col => col.ColumnName.Length);
      var maxDataTypeLen = this.Columns.Max(col => col.DataType.Length);
      for (var i = 0; i < this.Columns.Count; i++)
      {
        var column = this.Columns[i];
        if (i > 0)
        {
          streamWriter.WriteLine(",");
        }
        streamWriter.Write("  ");
        streamWriter.Write(column.ColumnName.PadRight(maxColNameLen));
        streamWriter.Write(" ");
        streamWriter.Write(column.DataType.PadRight(maxDataTypeLen));
        streamWriter.Write(" ");
        streamWriter.Write(column.Optional ? "null" : "not null");
        if (!string.IsNullOrWhiteSpace(column.DefaultValue))
        {
          if (!column.DefaultValue.StartsWith("identity"))
          {
            streamWriter.Write(" default");
          }
          streamWriter.Write(" ");
          streamWriter.Write(column.DefaultValue);
        }
      }
      streamWriter.WriteLine(")");
      streamWriter.WriteLine("go");
      streamWriter.WriteLine();
      streamWriter.WriteLine($@"execute sys.sp_addextendedproperty 'MS_Description', '{this.Description}' , 'SCHEMA', 'dbo', 'TABLE', '{this.TableName}'");
      streamWriter.WriteLine("go");
      streamWriter.WriteLine();
      foreach (var col in this.Columns)
      {
        streamWriter.WriteLine($@"execute sys.sp_addextendedproperty 'MS_Description', '{col.Description}', 'SCHEMA', 'dbo', 'TABLE', '{this.TableName}', 'COLUMN', '{col.ColumnName}'");
      }
      streamWriter.WriteLine(@"go");
      streamWriter.WriteLine();
    }

    public void WriteDropConstraintsCommands(StreamWriter streamWriter)
    {
      var cmds = new List<string>();

      if (this.ForeignKeys.Count > 0)
      {
        foreach (var foreignKey in this.ForeignKeys)
        {
          cmds.Add($@"alter table {this.TableName} drop constraint {foreignKey.ConstraintName}");
          cmds.Add("go");
        }
      }

      if (cmds.Count > 0)
      {
        for (int i = 0; i < cmds.Count; i++)
        {
          streamWriter.WriteLine(cmds[i]);
        }
      }
    }

    public void WriteAddConstraintsCommands(StreamWriter streamWriter)
    {
      var cmds = new List<string>();

      if (this.PrimaryKey != null)
      {
        cmds.Add(this.PrimaryKey.GetConstraintCommand());
      }

      if (this.ForeignKeys.Count > 0)
      {
        foreach (var foreignKey in this.ForeignKeys)
        {
          cmds.Add(foreignKey.GetConstraintCommand());
        }
      }

      if (this.Checks.Count > 0)
      {
        foreach (var check in this.Checks)
        {
          cmds.Add(check.GetConstraintCommand());
        }
      }

      if (cmds.Count > 0)
      {
        streamWriter.WriteLine($@"alter table [{this.TableName}]");
        streamWriter.WriteLine(@"add");
        for (int i = 0; i < cmds.Count; i++)
        {
          if (i > 0)
          {
            streamWriter.WriteLine(",");
          }
          streamWriter.Write("  ");
          streamWriter.Write(cmds[i]);
        }
        streamWriter.WriteLine();
        streamWriter.WriteLine("go");
        streamWriter.WriteLine();
      }
    }

    public void WriteDmlProcedures(StreamWriter streamWriter)
    {
      var insProc = new TableInsertProcedure(this);
      foreach (var cmd in insProc.GetSqlCommands())
      {
        streamWriter.WriteLine(cmd);
      }
      var updProc = new TableUpdateProcedure(this);
      foreach (var cmd in updProc.GetSqlCommands())
      {
        streamWriter.WriteLine(cmd);
      }
      var delProc = new TableDeleteProcedure(this);
      foreach (var cmd in delProc.GetSqlCommands())
      {
        streamWriter.WriteLine(cmd);
      }
    }

    public override string ToString()
    {
      return $@"Table ""{this.TableName}""";
    }
  }
}
