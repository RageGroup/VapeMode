﻿using System.Collections.Generic;
using System.Linq;
using System.IO;

namespace RelationalModelTool
{
  public sealed class TableDeleteProcedure: DbProcedureDefinition
  {
    public TableDefinition Table { get; }

    public TableDeleteProcedure(TableDefinition table) :
      base(table.TableName + "_Delete")
    {
      this.Table = table;
    }

    public override IReadOnlyList<string> GetSqlCommands()
    {
      if (this.Table.PrimaryKey == null)
      {
        return null;
      }

      var cmds = new List<string>();

      cmds.Add(this.GetDropObjectCmd());
      cmds.Add("go");

      cmds.Add($@"create procedure {this.ProcedureName}(");
      var parCols = this.Table.PrimaryKey.Columns;
      for (int i = 0; i < parCols.Count; i++)
      {
        var cmd = $@"  @{parCols[i].ColumnName} {parCols[i].DataType}";
        if (i == parCols.Count - 1)
        {
          cmds.Add(cmd + ")");
        }
        else
        {
          cmds.Add(cmd + ",");
        }
      }
      cmds.Add("as");
      cmds.Add("begin");
      cmds.Add("  begin try");
      cmds.Add($@"    delete from {this.Table.TableName}");
      for (int i = 0; i < parCols.Count; i++)
      {
        var cmd = $@"    {(i == 0 ? "where" : "     ")} {parCols[i].ColumnName} = @{parCols[i].ColumnName}";
        if (i < parCols.Count - 1)
        {
          cmd += " and";
        }
        cmds.Add(cmd);
      }
      cmds.Add("  end try");
      cmds.AddRange(GetGenericCatchBlock());
      cmds.Add("end");
      cmds.Add("go");
      cmds.Add(string.Empty);

      return cmds;
    }

    public override string ToString()
    {
      return $@"Table Delete Procedure for Table ""{this.Table.TableName}""";
    }
  }
}
