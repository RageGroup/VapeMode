﻿using System;
using System.Data;
using System.Data.Common;
using System.Data.OleDb;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using Excel = Microsoft.Office.Interop.Excel;

namespace RelationalModelTool
{
  public class ExcelWorksheet: DisposableBase
  {
    protected Excel.Worksheet _worksheet;

    internal ExcelWorksheet(Excel.Worksheet worksheet)
    {
      this._worksheet = worksheet;
      this.Cells = new ExcelCells(worksheet);
    }

    protected override void DisposeUnmanaged()
    {
      this.Cells.Dispose();
      Marshal.ReleaseComObject(this._worksheet);
      base.DisposeUnmanaged();
    }

    public ExcelCells Cells { get; }
  }
}
