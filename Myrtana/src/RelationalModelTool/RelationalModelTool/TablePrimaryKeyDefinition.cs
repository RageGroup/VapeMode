﻿using System.Collections.Generic;
using System.Text;

namespace RelationalModelTool
{
  public class TablePrimaryKeyDefinition: TableConstraintDefinition
  {
    public IReadOnlyList<TableColumnDefinition> Columns { get; }

    public TablePrimaryKeyDefinition(
      string tableName,
      IReadOnlyList<TableColumnDefinition> columns) :
      base("PK_" + tableName)
    {
      this.Columns = columns;
    }

    public override string GetConstraintCommand()
    {
      var cmd = new StringBuilder($@"constraint {this.ConstraintName} primary key (");
      for (int i = 0; i < this.Columns.Count; i++)
      {
        if (i > 0)
        {
          cmd.Append(", ");
        }
        cmd.Append(this.Columns[i].ColumnName);
      }
      cmd.Append(")");
      return cmd.ToString();
    }

    public override string ToString()
    {
      return $@"Primary Key ""{this.ConstraintName}""";
    }
  }
}
