﻿using System.Collections.Generic;
using System.Text;

namespace RelationalModelTool
{
  public struct TableForeignKeyColumnDefinition
  {
    public readonly string ColumnName;
    public readonly string ReferencedColumnName;

    public TableForeignKeyColumnDefinition(
      string columnName,
      string referencedColumnName)
    {
      this.ColumnName = columnName;
      this.ReferencedColumnName = referencedColumnName;
    }
  }

  public class TableForeignKeyDefinition: TableConstraintDefinition
  {
    public string ReferencedTableName { get; }
    public IReadOnlyList<TableForeignKeyColumnDefinition> Columns { get; }

    public TableForeignKeyDefinition(
      string tableName,
      string referencedTableName) :
      base("FK_" + tableName + "_" + referencedTableName)
    {
      this.ReferencedTableName = referencedTableName;
      this.Columns = new List<TableForeignKeyColumnDefinition>();
    }

    public override string GetConstraintCommand()
    {
      var cmd = new StringBuilder();
      cmd.Append($@"constraint {this.ConstraintName} foreign key (");
      int j;
      for (j = 0; j < this.Columns.Count; j++)
      {
        if (j > 0)
        {
          cmd.Append(", ");
        }
        cmd.Append(this.Columns[j].ColumnName);
      }
      cmd.Append($@") references [{this.ReferencedTableName}] (");
      for (j = 0; j < this.Columns.Count; j++)
      {
        if (j > 0)
        {
          cmd.Append(", ");
        }
        cmd.Append(this.Columns[j].ReferencedColumnName);
      }
      cmd.Append(")");
      return cmd.ToString();
    }

    public void AddColumn(string columnName, string referencedColumnName)
    {
      ((List<TableForeignKeyColumnDefinition>) this.Columns).Add(
        new TableForeignKeyColumnDefinition(columnName, referencedColumnName));
    }

    public override string ToString()
    {
      return $@"Foreign Key ""{this.ConstraintName}""";
    }
  }
}
