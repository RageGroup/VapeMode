﻿using System;
using System.Data;
using System.Data.Common;
using System.Data.OleDb;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using Excel = Microsoft.Office.Interop.Excel;

namespace RelationalModelTool
{
  public static class Program
  {
    public static void Main(string[] args)
    {
      var tables = new List<TableDefinition>();
      using (var workbook = ExcelApplication.Instance.OpenWorkbook(ExcelFile))
      {
        foreach (var worksheet in workbook.Worksheets)
        {
          tables.Add(new TableDefinition(worksheet.Cells));
        }
      }
      CreateSqlFiles(tables);
    }

    public static void CopyFileContent(StreamWriter trgFileStream, string filepath)
    {
      int count;
      const int size = 1024;
      char[] buffer = new char[size];
      using (var fileStream = File.OpenRead(Path.GetFullPath(filepath)))
      {
        using (var streamReader = new StreamReader(fileStream, Encoding.UTF8))
        {
          for (; ; )
          {
            count = streamReader.Read(buffer, 0, size);
            if (count == 0)
            {
              break;
            }
            trgFileStream.Write(buffer, 0, count);
          }
        }
      }
    }

    public static void CreateSqlFiles(List<TableDefinition> tables)
    {
      using (var fileStream = File.Create(Path.GetFullPath(CreateTablesSqlFile)))
      {
        using (var streamWriter = new StreamWriter(fileStream, Encoding.UTF8))
        {
          streamWriter.AutoFlush = true;
          streamWriter.WriteLine(@"use [Myrtana]");
          streamWriter.WriteLine(@"go");
          streamWriter.WriteLine();
          tables.WriteCreateTableCommands(streamWriter);
          streamWriter.WriteLine("/*");
          tables.WriteDropConstraintsCommands(streamWriter);
          streamWriter.WriteLine("--*/");
          streamWriter.WriteLine();
          tables.WriteAddConstraintsCommands(streamWriter);
        }
      }
      using (var fileStream = File.Create(Path.GetFullPath(CreateProceduresSqlFile)))
      {
        using (var streamWriter = new StreamWriter(fileStream, Encoding.UTF8))
        {
          streamWriter.AutoFlush = true;
          streamWriter.WriteLine(@"use [Myrtana]");
          streamWriter.WriteLine(@"go");
          streamWriter.WriteLine();
          tables.WriteDmlProcedures(streamWriter);
        }
      }
      using (var fileStream = File.Create(Path.GetFullPath(CreateProceduresSqlFile)))
      {
        using (var streamWriter = new StreamWriter(fileStream, Encoding.UTF8))
        {
          streamWriter.AutoFlush = true;
          streamWriter.WriteLine(@"use [Myrtana]");
          streamWriter.WriteLine(@"go");
          streamWriter.WriteLine();
          tables.WriteDmlProcedures(streamWriter);
        }
      }
      using (var fileStream = File.Create(Path.GetFullPath(CreateAllSqlFile)))
      {
        using (var streamWriter = new StreamWriter(fileStream, Encoding.UTF8))
        {
          CopyFileContent(streamWriter, CreateTablesSqlFile);
          var directories = Directory.EnumerateDirectories(WorkingDirPath);
          foreach (var directory in directories)
          {
            if (!directory.Contains("Skript-Updates")) {
              var filepaths = Directory.EnumerateFiles(directory, "*.sql");
              foreach (var filepath in filepaths)
              {
                CopyFileContent(streamWriter, filepath);
                streamWriter.WriteLine();
              }
            }
          }
        }
      }
    }

    public const string ExcelFile = @"..\..\..\..\..\doc\Relational Model\Myrtana-RM.xlsx";
    public const string WorkingDirPath = @"..\..\..\..\";
    public const string CreateAllSqlFile = @"..\..\..\..\Generated-Create-All.sql";
    public const string CreateTablesSqlFile = @"..\..\..\..\Generated-Create-Tables.sql";
    public const string CreateProceduresSqlFile = @"..\..\..\..\Generated-Create-Procedures.sql";
  }
}
