﻿using System;

namespace RelationalModelTool
{
  public class TableColumnDefinition: IEquatable<TableColumnDefinition>
  {
    public int ColumnId { get; }
    public string ColumnName { get; }
    public string Description { get; }
    public string DataType { get; }
    public bool Optional { get; }
    public string DefaultValue { get; }
    public bool PrimaryKey { get; }
    public string ForeignKeyTable { get; }
    public string ForeignKeyColumn { get; }
    public string Checks { get; }

    public TableColumnDefinition(
      int columnId,
      string columnName,
      string description,
      string dataType,
      bool optional,
      string defaultValue,
      bool primaryKey,
      string foreignKeyTable,
      string foreignKeyColumn,
      string checks)
    {
      this.ColumnId = columnId;
      this.ColumnName = columnName;
      description = description.Replace(",\r\n", ", ").Replace(",\r", ", ").Replace(",\n", ", ");
      description = description.Replace("\r\n", "").Replace("\r", "").Replace("\n", "");
      description = description.Replace("'", "''");
      this.Description = description;
      this.DataType = dataType;
      this.Optional = optional;
      this.DefaultValue = defaultValue;
      this.PrimaryKey = primaryKey;
      this.ForeignKeyTable = foreignKeyTable;
      this.ForeignKeyColumn = foreignKeyColumn;
      this.Checks = checks;
    }

    public bool Equals(TableColumnDefinition other)
    {
      return (this.ColumnName == other.ColumnName);
    }

    public override string ToString()
    {
      return $@"Table Column ""{this.ColumnName}""";
    }
  }
}
