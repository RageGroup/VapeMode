﻿using System.Collections.Generic;
using System.Linq;
using System.IO;

namespace RelationalModelTool
{
  public static class TableDefinitionExtensions
  {
    public static void WriteCreateTableCommands(this IEnumerable<TableDefinition> tables, StreamWriter streamWriter)
    {
      foreach (var table in tables)
      {
        table.WriteCreateTableCommands(streamWriter);
      }
    }

    public static void WriteDropConstraintsCommands(this IEnumerable<TableDefinition> tables, StreamWriter streamWriter)
    {
      foreach (var table in tables)
      {
        table.WriteDropConstraintsCommands(streamWriter);
      }
    }

    public static void WriteAddConstraintsCommands(this IEnumerable<TableDefinition> tables, StreamWriter streamWriter)
    {
      foreach (var table in tables)
      {
        table.WriteAddConstraintsCommands(streamWriter);
      }
    }

    public static void WriteDmlProcedures(this IEnumerable<TableDefinition> tables, StreamWriter streamWriter)
    {
      foreach (var table in tables)
      {
        table.WriteDmlProcedures(streamWriter);
      }
    }
  }
}
