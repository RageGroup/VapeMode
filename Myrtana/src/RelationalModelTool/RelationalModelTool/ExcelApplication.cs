﻿using System;
using System.Data;
using System.Data.Common;
using System.Data.OleDb;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using Excel = Microsoft.Office.Interop.Excel;

namespace RelationalModelTool
{
  public class ExcelApplication: DisposableBase
  {
    protected Excel.Application _app;

    protected ExcelApplication()
    {
      this._app = new Excel.Application();
      this._workbooks = new DisposableCollection<ExcelWorkbook>();
      this.Workbooks = new ReadOnlyCollection<ExcelWorkbook>(_workbooks);
    }

    protected override void DisposeUnmanaged()
    {
      this._app.Quit();
      Marshal.ReleaseComObject(this._app);
      base.DisposeUnmanaged();
    }

    public ExcelWorkbook OpenWorkbook(string excelFilepath)
    {
      var workbook = new ExcelWorkbook(_app.Workbooks.Open(
        Path.GetFullPath(excelFilepath)
        //,
        //0, true, 5, "", "", true, Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0
        ));
      this._workbooks.Add(workbook);
      return workbook;
    }

    protected DisposableCollection<ExcelWorkbook> _workbooks;
    public IReadOnlyList<ExcelWorkbook> Workbooks { get; protected set; }

    public static ExcelApplication Instance { get; } = new ExcelApplication();
  }
}
