﻿using System.Collections.Generic;
using System.Linq;
using System.IO;

namespace RelationalModelTool
{
  public abstract class DbProcedureDefinition: DbObjectDefinition
  {
    public string ProcedureName => this.ObjectName;

    protected DbProcedureDefinition(string procedureName):
      base("P", "P_" + procedureName)
    {
    }

    public static IReadOnlyList<string> GetGenericCatchBlock()
    {
      var cmds = new List<string>();
      cmds.Add(@"  begin catch");
      cmds.Add(@"    declare @errno int");
      cmds.Add(@"    declare @errmsg varchar(256)");
      cmds.Add(@"    select  @errno  = error_number(),");
      cmds.Add(@"            @errmsg = error_message()");
      cmds.Add(@"    print concat('Fehler (', @errno, '): ', @errmsg)");
      cmds.Add(@"  end catch");
      return cmds;
    }

    public override string ToString()
    {
      return $@"Database Procedure ""{this.ProcedureName}""";
    }
  }
}
