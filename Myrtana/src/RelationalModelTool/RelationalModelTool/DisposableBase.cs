﻿using System;

namespace RelationalModelTool
{
  /// <summary>
  /// A base implementation for disposable objects.
  /// </summary>
  /// <remarks>
  /// The members are mostly virtual so that they are suitable for interception.
  /// </remarks>
  public abstract class DisposableBase: IDisposable
  {
    #region Constructors
    /// <summary>
    /// Construct the base for disposable objects.
    /// </summary>
    public DisposableBase()
    {
      this._disposed = false;
    }

    /// <summary>
    /// Finalize the base for disposable objects.
    /// </summary>
    ~DisposableBase()
    {
      this.Dispose(false);
    }
    #endregion Constructors

    #region Methods
    /// <summary>
    /// Explicitly dispose managed and unmanaged resources if necessary.
    /// </summary>
    public void Dispose()
    {
      this.Dispose(true);
      GC.SuppressFinalize(this);
    }

    /// <summary>
    /// Dispose unmanaged resources. Will be called at most once when explicitly disposing or when finalizing.
    /// </summary>
    protected virtual void DisposeUnmanaged()
    {
    }

    /// <summary>
    /// Dispose managed resources. Will be called at most once when explicitly disposing but not when finalizing.
    /// </summary>
    protected virtual void DisposeManaged()
    {
    }

    /// <summary>
    /// Dispose managed and unmanaged resources if necessary.
    /// </summary>
    /// <param name="disposing">Explicit disposal requested? False when finalizing.</param>
    protected virtual void Dispose(bool disposing)
    {
      if (!this._disposed)
      {
        if (disposing)
        {
          this.DisposeManaged();
        }
        this.DisposeUnmanaged();
        this._disposed = true;
        var disposed = this.Disposed;
        if (disposed != null)
        {
          disposed(this, EventArgs.Empty);
        }
      }
    }
    #endregion Methods

    #region Events
    /// <summary>
    /// Event that is fired, when the object has been disposed.
    /// </summary>
    public virtual event EventHandler Disposed;
    #endregion Events

    #region Properties
    /// <summary>
    /// Has the object been disposed?
    /// </summary>
    public virtual bool IsDisposed
    {
      get { return this._disposed; }
    }
    #endregion Properties

    #region Fields
    /// <summary>
    /// Has the object been disposed?
    /// </summary>
    private bool _disposed;
    #endregion Fields
  }
}
