﻿using System;
using System.Data;
using System.Data.Common;
using System.Data.OleDb;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using Excel = Microsoft.Office.Interop.Excel;

namespace RelationalModelTool
{
  public class ExcelWorkbook: DisposableBase
  {
    protected Excel.Workbook _workbook;

    internal ExcelWorkbook(Excel.Workbook workbook)
    {
      this._workbook = workbook;
      this._worksheets = new DisposableCollection<ExcelWorksheet>();
      this.Worksheets = new ReadOnlyCollection<ExcelWorksheet>(_worksheets);
      var worksheets = workbook.Worksheets;
      for (int i = 1; i <= worksheets.Count; i++)
      {
        this._worksheets.Add(new ExcelWorksheet((Excel.Worksheet) worksheets.get_Item(i)));
      }
      Marshal.ReleaseComObject(worksheets);
    }

    protected override void DisposeUnmanaged()
    {
      this._worksheets.Dispose();
      Marshal.ReleaseComObject(this._workbook);
      base.DisposeUnmanaged();
    }

    protected DisposableCollection<ExcelWorksheet> _worksheets;
    public IReadOnlyList<ExcelWorksheet> Worksheets { get; protected set; }
  }
}
