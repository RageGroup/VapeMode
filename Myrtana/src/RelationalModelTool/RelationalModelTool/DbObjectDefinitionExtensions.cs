﻿using System.Collections.Generic;
using System.Linq;
using System.IO;

namespace RelationalModelTool
{
  public static class DbObjectDefinitionExtension
  {
    public static IReadOnlyList<string> GetSqlCommands(this IEnumerable<DbObjectDefinition> dbObjects)
    {
      var cmds = new List<string>();
      foreach (var dbObject in dbObjects)
      {
        cmds.AddRange(dbObject.GetSqlCommands());
      }
      return cmds;
    }
  }
}
