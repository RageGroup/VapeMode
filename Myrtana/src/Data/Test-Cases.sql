use [Myrtana]
go

--------------------------------------------------------------------------------
-- Testszenarien zum Erstellen von Kunden

execute dbo.P_CreateCustomer 'Theatergruppe 1349', 'Hans Meier', 25598, 'Erfurt', 'Südstrasse 1'

--------------------------------------------------------------------------------
-- Testszenarien zum Erstellen von Mitarbeitern

execute dbo.P_CreateEmployee 'Max', 'Mustermann', '54545', 'Trier','Karl-Marx-Str. 1', 255.99, 2, 'B'

--------------------------------------------------------------------------------
-- Löschprozesse zu den Kunden/Mitarbeiterdatensätze

-- Löschung eines Kundens mit der entsprechenden ID
execute dbo.P_DeleteCustomer 2

-- Löschung eines Mitarbeiters mit der entsprechenden ID
execute dbo.P_DeleteEmployee 14

--------------------------------------------------------------------------------
-- Update-Prozesse der Kunden/Mitarbeiterdatensätze

-- Änderung des Wohnsitzes
execute dbo.P_UpdateCustomer 4, @City = 'Berlin'

-- Änderung des Namens, des Manager Tags sowie der Vergütung
execute dbo.P_UpdateEmployee 5, @LastName = 'Hansen', @EmployeeType = 'M', @Salary = 339.00

--------------------------------------------------------------------------------
-- Produkt-Erstellung

execute dbo.P_CreateProduct 'Schwert für Kinder', 'Me', 30.0, 1200, @ProductCategory = 'W', @WeaponType = 'Sc', @WeaponSize = 'M'
execute dbo.P_CreateProduct 'Lederrüstung für Kinder', 'Le', 800.0, 1000, 'R', @ArmorPart = 'Se'

--------------------------------------------------------------------------------
-- Produkt-Änderung

-- Änderungen des Lagerbestandes mit der ID 13
execute dbo.P_UpdateProduct 13, @Stock = 0

-- Änderungen des Lagerbestandes mit der ID 11
execute dbo.P_UpdateProduct 11, @Stock = 5

--------------------------------------------------------------------------------
-- Produkt-Löschung

-- Entfernung des Produktes mit der ID 13 aus dem Sortiment
execute dbo.P_DeleteProduct 13

--------------------------------------------------------------------------------

print '--------------------'
print '-- Function-Tests:'

print concat('FN_CalcProductionPrice(1): ', dbo.FN_CalcProductionPrice(1))

print concat('FN_CalcRepair(64):  ', dbo.FN_CalcRepair(1, 63))
print concat('FN_CalcRepair(100): ', dbo.FN_CalcRepair(1, 100))

--------------------------------------------------------------------------------
-- Kurs-Erstellung

-- Erstellung der Training Kurse
execute dbo.P_CreateCourse 'Training-Anfänger', 100, 1

--------------------------------------------------------------------------------
-- Kurs-Änderungen

-- Änderung des Kursnamens
execute dbo.P_UpdateCourse 1, 'Schwerttraining-Grünschnabel', 100, 1

-- Änderung des Kursnamens
execute dbo.P_UpdateCourse 2, 'Schwerttraining-Knappe', 200, 3

--------------------------------------------------------------------------------
-- Kurs-Löschung

-- Löscht den Kurs mit der ID 1
execute dbo.P_DeleteCourse 1

--------------------------------------------------------------------------------
-- Bestellungen anlegen
-- (neuer Bestellkopf, neue Bestellposition, Bestellung abschließen)
execute P_NewOrder @CustomerId = 1
go

execute P_CreateOrderItem 2, @ProductId = 1, @Quantity = 13, @IsRepair = 0
go
execute P_CreateOrderItem 2, @ProductId = 1, @Quantity = 7, @IsRepair = 1
go
execute P_CreateOrderItem 2, @CourseId = 2, @ParticipantCount = 15
go

execute P_SetOrderState 2, 'A'
go

--------------------------------------------------------------------------------
