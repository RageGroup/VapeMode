--------------------------------------------------------------------------------
set identity_insert [Employees] on
go

insert into Employees (EmployeeId, FirstName, LastName, Postcode, City, StreetHouseNo, HireDate, Salary, Manager, EmployeeType)
values
  (1, 'Pain', 'Fabian', '49099', 'Erfurt', 'Malburger Platz 3', '2018-07-12', 215.99, null, 'M'),
  (2, 'Savas', 'Peter', '08351', 'Berlin', 'Goethestr. 2', '2018-07-12', 215.99, null, 'M')
go

set identity_insert [Employees] off
go

--------------------------------------------------------------------------------
set identity_insert [Customers] on
go

insert into Customers (CustomerId, CustomerName, Contact, Postcode, City, StreetHouseNo)
values
  (1, 'Hugh Anderson', 'Rubber Duck', '99086', 'Erfurt', 'Juri-Gagarin-Ring. 3'),
  (2, 'John Anderson', 'Phil Dunphy', '99089', 'Erfurt', 'Schapirostraße. 10'),
  (3, 'Andreas Malikum', 'John Anderson', '99086', 'Erfurt', 'Berliner-Straße. 9'),
  (4, 'Phil Dunphy', 'John Anderson', '85051', 'Berlin', 'Münchener-Straße. 9'),
  (5, 'Andrea Cambiolo', 'John Anderson', '99086', 'Erfurt', 'Leipziger-Platz. 9'),
  (6, 'Felix Huber', 'John Anderson', '99088', 'Erfurt', 'Grünwald-Str. 85'),
  (7, 'Peter Lustig', 'Laura Dark', '69053', 'Fürth', 'Berliner-Straße. 9'),
  (8, 'Laura Dark', 'Robin Vollmer', '99086', 'Erfurt', 'Daberstedt-Str. 2'),
  (9, 'Pascal Levain', 'John Anderson', '99086', 'Erfurt', 'Am DMV. 25'),
  (10, 'Andre Levain', 'John Anderson', '25598', 'Los Santos', 'Grovestreet. 1')
go

set identity_insert [Customers] off
go

--------------------------------------------------------------------------------
set identity_insert [Products] on
go

insert into Products (ProductId, ProductName, Material, Stock, ProductCategory, UnitPrice, WeaponType, WeaponSize, ArmorPart)
values
  (1, 'Kleiner Holzdolch', 'Ho', 1180, 'W', 30.00, 'Do', 'S', null),
  (2, 'Holzdolch', 'Ho', 1200, 'W', 35.00, 'Do', 'M', null),
  (3, 'Großer Holzdolch', 'Ho', 1200, 'W', 40.00, 'Do', 'L', null),
  (4, 'Kleiner Metalldolch', 'Me', 1200, 'W', 150.00, 'Do', 'S', null),
  (5, 'Metalldolch', 'Me', 1200, 'W', 160.00, 'Do', 'M', null),
  (6, 'Großer Metalldolch', 'Me', 1200, 'W', 170.00, 'Do', 'L', null),
  (7, 'Kleine Holzaxt', 'Ho', 1200, 'W', 40.00, 'Ax', 'S', null),
  (8, 'Holzaxt', 'Ho', 1200, 'W', 45.00, 'Ax', 'M', null),
  (9, 'Große Holzaxt', 'Ho', 1200, 'W', 50.00, 'Ax', 'L', null),
  (10, 'Kleine Metallaxt', 'Me', 1200, 'W', 190.00, 'Ax', 'S', null),
  (11, 'Metallaxt', 'Me', 5, 'W', 200.00, 'Ax', 'M', null),
  (12, 'Große Metallaxt', 'Me', 1200, 'W', 210.00, 'Ax', 'L', null),
  (13, 'Kleines Holzschwert', 'Ho', 0, 'W', 50.00, 'Sc', 'S', null),
  (14, 'Holzschwert', 'Ho', 1200, 'W', 55.00, 'Sc', 'M', null),
  (15, 'Großes Holzschwert', 'Ho', 1200, 'W', 60.00, 'Sc', 'L', null),
  (16, 'Kleines Metallschwert', 'Me', 1200, 'W', 200.00, 'Sc', 'S', null),
  (17, 'Metallschwert', 'Me', 1200, 'W', 225.00, 'Sc', 'M', null),
  (18, 'Großes Metallschwert', 'Me', 1200, 'W', 250.00, 'Sc', 'L', null),
  (19, 'Kleines Holzlangschwert', 'Ho', 1200, 'W', 60.00, 'Sc', 'S', null),
  (20, 'Holzlangschwert', 'Ho', 1200, 'W', 65.00, 'Sc', 'M', null),
  (21, 'Großes Holzlangschwert', 'Ho', 1200, 'W', 70.00, 'Sc', 'L', null),
  (22, 'Kleines Metalllangschwert', 'Me', 1200, 'W', 250.00, 'Sc', 'S', null),
  (23, 'Metalllangschwert', 'Me', 1200, 'W', 275.00, 'Sc', 'M', null),
  (24, 'Großes Metalllangschwert', 'Me', 1200, 'W', 300.00, 'Sc', 'L', null),
  (25, 'Kleiner Bogen', 'Ho', 1200, 'W', 70.00, 'Sc', 'S', null),
  (26, 'Bogen', 'Ho', 1200, 'W', 75.00, 'Sc', 'M', null),
  (27, 'Großer Bogen', 'Ho', 1200, 'W', 80.00, 'Sc', 'L', null),
  (28, 'Kleine Armbrust', 'Ho', 1200, 'W', 70.00, 'Sc', 'S', null),
  (29, 'Armbrust', 'Ho', 1200, 'W', 75.00, 'Sc', 'M', null),
  (30, 'Große Armbrust', 'Ho', 1200, 'W', 80.00, 'Sc', 'L', null),
  (31, 'Stoffhelm', 'St', 1000, 'R', 40.00, null, null, 'He'),
  (32, 'Stoffbrustrüstung', 'St', 1000, 'R', 150.00, null, null, 'Br'),
  (33, 'Stoffhandschuh', 'St', 1000, 'R', 50.00, null, null, 'Ha'),
  (34, 'Stoffbeinrüstung', 'St', 1000, 'R', 130.00, null, null, 'Be'),
  (35, 'Stoffschuh', 'St', 1000, 'R', 70.00, null, null, 'Sc'),
  (36, 'Lederhelm', 'Le', 1000, 'R', 40.00, null, null, 'He'),
  (37, 'Lederbrustrüstung', 'Le', 1000, 'R', 150.00, null, null, 'Br'),
  (38, 'Lederhandschuh', 'Le', 1000, 'R', 50.00, null, null, 'Ha'),
  (39, 'Lederbeinrüstung', 'Le', 1000, 'R', 130.00, null, null, 'Be'),
  (40, 'Lederschuh', 'Le', 1000, 'R', 70.00, null, null, 'Sc'),
  (41, 'Plattenhelm', 'Pl', 1000, 'R', 40.00, null, null, 'He'),
  (42, 'Plattenbrustrüstung', 'Pl', 1000, 'R', 150.00, null, null, 'Br'),
  (43, 'Plattenhandschuh', 'Pl', 1000, 'R', 50.00, null, null, 'Ha'),
  (44, 'Plattenbeinrüstung', 'Pl', 1000, 'R', 130.00, null, null, 'Be'),
  (45, 'Plattenschuh', 'Pl', 1000, 'R', 70.00, null, null, 'Sc'),
  (46, 'Stoffrüstungsset', 'St', 1000, 'R', 400.00, null, null, 'Se'),
  (47, 'Lederrüstungsset', 'Le', 1000, 'R', 800.00, null, null, 'Se'),
  (48, 'Plattenrüstungsset', 'Pl', 1000, 'R', 1200.00, null, null, 'Se'),
  (49, 'Schwert für Kinder', 'Me', 1200, 'W', 30.00, 'Sc', 'M', null),
  (50, 'Lederrüstung für Kinder', 'Le', 1000, 'R', 800.00, null, null, 'Se')
go

set identity_insert [Products] off
go

--------------------------------------------------------------------------------
set identity_insert [Courses] on
go

insert into Courses (CourseId, CourseName, PricePerTrainer, Duration)
values
  (1, 'Schwerttraining-Grünschnabel', 100.00, 1),
  (2, 'Schwerttraining-Knappe', 200.00, 3),
  (3, 'Schwerttraining-Experte', 250.00, 5),
  (4, 'Sprachtraining-Anfänger', 75.00, 1),
  (5, 'Sprachtraining-Fortgeschritten', 150.00, 3),
  (6, 'Sprachtraining-Experte', 200.00, 5)
go

set identity_insert [Courses] off
go

--------------------------------------------------------------------------------
set identity_insert [Orders] on
go

insert into Orders (OrderId, OrderDate, OrderState, CustomerId, Price)
values
  (1, '2018-07-12', 'A', 1, 1000.00)
go

set identity_insert [Orders] off
go

--------------------------------------------------------------------------------
insert into OrderItems (OrderId, ProductId, Quantity, UnitPrice, IsRepair, CourseId, ParticipantCount)
values (1, 1, 13, 30.00, 0, null, null)
go

insert into OrderItems (OrderId, ProductId, Quantity, UnitPrice, IsRepair, CourseId, ParticipantCount)
values (1, 1, 7, 30.00, 1, null, null)
go

insert into OrderItems (OrderId, ProductId, Quantity, UnitPrice, IsRepair, CourseId, ParticipantCount)
values (1, null, null, null, null, 2, 15)
go

--------------------------------------------------------------------------------
