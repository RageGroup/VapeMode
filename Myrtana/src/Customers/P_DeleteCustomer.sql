use [Myrtana]
go

if object_id('P_DeleteCustomer', 'P') is not null drop procedure P_DeleteCustomer
go

create procedure P_DeleteCustomer(@CustomerId int)
as
begin

  begin try
    if not exists (select * from Customers where @CustomerId = CustomerId)
      throw 50001, 'Ein Kunde mit dieser Nummer existiert nicht.', 1

    delete from Customers
    where CustomerId = @CustomerId

    print concat('Der Kunde mit der Nummer ', @CustomerId, ' wurde erfolgreich gelöscht.')
  end try

  begin catch
    if error_number() = 50001 begin
      print concat ('Der Kunde ', @CustomerId, ' existiert nicht.')
    end
    else begin
      print error_message()
    end
  end catch

end
go
