use [Myrtana]
go

if object_id('P_UpdateCustomer', 'P') is not null drop procedure P_UpdateCustomer
go

create procedure P_UpdateCustomer(
  @CustomerId     int,
  @CustomerName   varchar(80) = null,
  @Contact        varchar(80) = null,
  @Postcode       varchar(5) = null,
  @City           varchar(50) = null,
  @StreetHouseNo  varchar(80) = null)
as
begin
  begin try
    if not exists (select * from Customers where @CustomerId = CustomerId)
      throw 50002, 'Ein Kunde mit dieser Nummer existiert nicht.', 1

    update  Customers
    set     CustomerName  = coalesce(@CustomerName, CustomerName),
            Contact       = coalesce(@Contact, Contact),
            Postcode      = coalesce(@Postcode, Postcode),
            City          = coalesce(@City, City),
            StreetHouseNo = coalesce(@StreetHouseNo, StreetHouseNo)
    where CustomerId = @CustomerId

    select  @CustomerName = CustomerName
    from    Customers
    where   @CustomerId = CustomerId
    print concat('Die Kundendaten von (', @CustomerName, ') wurden erfolgreich editiert.')
  end try

  begin catch
    if error_number() = 50002 begin
      print concat('Der Kunde mit der Nummer ', @CustomerId, ' existiert nicht.')
    end
    else begin
      print error_message()
    end
  end catch
end
go
