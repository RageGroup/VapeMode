use [Myrtana]
go

if object_id('P_CreateCustomer', 'P') is not null drop procedure P_CreateCustomer
go

create procedure P_CreateCustomer(
  @CustomerName   varchar(80),
  @Contact        varchar(80),
  @Postcode       varchar(5),
  @City           varchar(50),
  @StreetHouseNo  varchar(80))
as
begin

  begin try
    if exists (select * from Customers where @CustomerName = CustomerName)
      throw 50001, 'Ein Kunde mit diesem Namen existiert bereits', 1

    insert into Customers (CustomerName, Contact, Postcode, City, StreetHouseNo)
    values (@CustomerName, @Contact, @Postcode, @City, @StreetHouseNo)

    print concat('Der Kunde ''', @CustomerName, ''' wurde erfolgreich angelegt.')
  end try

  begin catch
    if error_number() = 50001 begin
      print concat('Der Kunde mit dem Namen ', @CustomerName, ' existiert bereits.')
    end
    else begin
      print error_message()
    end
  end catch

end
go
