if object_id('FN_CalcRepair', 'FN') is not null drop function [FN_CalcRepair]
go

create function FN_CalcRepair(@ProductId int, @RepairPercent tinyint)
returns decimal(8, 2)
as
begin
  declare @RepairCost decimal(8, 2)
  declare @actualPrice decimal(8, 2)
  declare @RepairContent decimal(5, 3)

  if @RepairPercent >= 100 begin
    set @RepairCost = -1.0
  end
  else begin
    set @RepairContent = 63.0 / 100.0 * @RepairPercent

    select  @actualPrice = UnitPrice
    from    Products
    where   ProductId = @ProductId

    set @RepairCost = (@actualPrice / 100.0) * @RepairContent
  end

  return @RepairCost
end
go
