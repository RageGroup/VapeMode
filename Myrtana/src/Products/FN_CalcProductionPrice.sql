if object_id('FN_CalcProductionPrice', 'FN') is not null drop function [FN_CalcProductionPrice]
go

create function FN_CalcProductionPrice(@ProductId int) returns decimal(8, 2)
as
begin
  declare @UnitPrice decimal(8,2)
  declare @ProductionPrice decimal(8,2)

  select  @UnitPrice = UnitPrice
  from    Products
  where   ProductId = @ProductId

  set @ProductionPrice = (@UnitPrice / 100.0) * 63.0

  return @ProductionPrice
end
go
