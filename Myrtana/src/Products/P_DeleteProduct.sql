use [Myrtana]
go

if object_id('P_DeleteProduct', 'P') is not null drop procedure [P_DeleteProduct]
go

create procedure P_DeleteProduct(@ProductId int)
as
begin

  begin try
    declare @Stock int

    select  @Stock = Stock
    from    Products
    where   @ProductId = ProductId

    if not exists (select * from Products where @ProductId = ProductId) throw 65005, '', 1

    if @Stock > 0 throw 65006, '', 1

    delete from Products where ProductId = @ProductId

    print concat('Das Produkt mit der Nummer ', @ProductId, ' wurde erfolgreich gelöscht.')
  end try

  begin catch
    if error_number() = 65005 begin
      print concat('Ein Produkt mit der Nummer ', @ProductId, ' existiert nicht.')
    end
    else if error_number() = 65006 begin
      print concat('Das Produkt mit der Nummer ', @ProductId, ' kann nicht gelöscht werden, da noch ', @Stock, ' Stück auf Lager sind.')
    end
    else begin
      print error_message()
    end
  end catch

end
go
