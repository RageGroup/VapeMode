use [Myrtana]
go

if object_id('P_UpdateProduct', 'P') is not null drop procedure [P_UpdateProduct]
go

create procedure P_UpdateProduct(
  @ProductId        int,
  @ProductName      varchar(80) = null,
  @Material         char(2)     = null,
  @Stock            int         = null,
  @ProductCategory  char(1)     = null,
  @WeaponType       char(2)     = null,
  @WeaponSize       char(1)     = null,
  @ArmorPart        char(2)     = null)
as
begin

  begin try
    declare @cat_old char(1)

    select @cat_old = ProductCategory from Products

    if not exists (select * from Products where @ProductId = ProductId) throw 65004, '', 1
    if @cat_old != @ProductCategory and @ProductCategory is not null throw 65007, '', 1

    if @cat_old = 'W' begin
      update Products
      set   ProductName = coalesce(@ProductName, ProductName),
            Material    = coalesce(@Material, Material),
            Stock       = coalesce(@Stock, Stock),
            WeaponType  = coalesce(@WeaponType, WeaponType),
            WeaponSize  = coalesce(@WeaponSize, WeaponSize)
      where ProductId   = @ProductId
    end
    else if @cat_old = 'R' begin
      update  Products
        set   ProductName = coalesce(@ProductName, ProductName),
              Material    = coalesce(@Material, Material),
              Stock       = coalesce(@Stock, Stock),
              ArmorPart   = coalesce(@ArmorPart, ArmorPart)
        where ProductId   = @ProductId
    end
    else begin
      update Products
      set   ProductName = coalesce(@ProductName, ProductName),
            Material = coalesce(@Material, Material),
          Stock = coalesce(@Stock, Stock)
      where ProductId = @ProductId
    end

    select  @ProductName = ProductName
    from    Products
    where   @ProductId = ProductId

    print concat ('Das Produkt mit der Nummer ', @ProductId, ' wurde erfolgreich geändert.')
  end try

  begin catch
    if error_number() = 65004 begin
      print concat('Ein Produkt mit der Nummer ', @ProductId, ' existiert nicht.')
    end
    else if error_number() = 65007 begin
      print 'Die Produkkategorie kann nicht geändert werden.'
    end
    else begin
      print error_message()
    end
  end catch

end
go
