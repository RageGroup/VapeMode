use [Myrtana]
go

if object_id('P_CreateProduct', 'P') is not null drop procedure [P_CreateProduct]
go

create procedure P_CreateProduct(
  @ProductName      varchar(80),
  @Material         char(2),
  @UnitPrice        decimal(8, 2),
  @Stock            int,
  @ProductCategory  char(1),
  @WeaponType       char(2) = null,
  @WeaponSize       char(1) = null,
  @ArmorPart        char(2) = null)
as
begin

  begin try
    if exists (select * from Products where @ProductName = ProductName) throw 65000, 'Produkt vorhanden', 1
    if @ProductCategory not in ('W', 'R', 'P') throw 65003, 'Keine existiertende Kategorie', 1

    if @ProductCategory = 'W' begin
      if @ArmorPart is not null throw 65001, 'Waffe != Rüstung', 1
      if @WeaponType not in ('Do', 'Ax', 'Sc', 'La', 'Bo', 'Ar') throw 65008, 'Waffentyp existiert nicht', 1
      if @WeaponSize not in ('S', 'M', 'L') throw 65009, 'Waffengröße existiert nicht', 1
      if @Material not in ('Ho', 'Me') throw 65010, 'Waffenmatrial existiert nicht', 1

      insert into Products (ProductName, Material, Stock, ProductCategory, UnitPrice, WeaponType, WeaponSize)
      values (@ProductName, @Material, @Stock, @ProductCategory, @UnitPrice, @WeaponType, @WeaponSize)
    end
    else if @ProductCategory = 'R' begin
      if @WeaponSize is not null or @WeaponType is not null throw 65002, 'Rüstung != Waffe', 1
      if @Material not in ('St', 'Le', 'Ke', 'Pl') throw 65011, 'Rüstungsmaterial existiert nicht', 1
      if @ArmorPart not in ('He', 'Br', 'Ha', 'Be', 'Sc', 'Se') throw 65012, 'Rüstungsteil existiert nicht',1
      if @Material = 'Ke' and @ArmorPart = 'Se' throw 65013, 'Kettenset existiert nicht', 1

      insert into Products (ProductName, Material, Stock, ProductCategory, UnitPrice, ArmorPart)
      values (@ProductName, @Material, @Stock, @ProductCategory, @UnitPrice, @ArmorPart)
    end
    else begin
      insert into Products (ProductName, Material, Stock, ProductCategory)
      values (@ProductName, @Material, @Stock, @ProductCategory)
    end

    print concat('Das Produkt mit dem Namen ', @ProductName, ' wurde erfolgreich erstellt.')
  end try

  begin catch
    if error_number() = 65000 begin
      print concat('Das Produkt ', @ProductName, ' existiert bereits.')
    end
    else if error_number() in (65001, 65002) begin
      print concat('Das Produkt mit dem Namen ', @ProductName, ' wurde als falscher Typ deklariert.')
    end
    else if error_number() = 65003 begin
      print 'Die eingegebene Kategorie existiert nicht.'
    end
    else if error_number() = 65008 begin
      print 'Der Waffentyp existiert nicht'
    end
    else if error_number() = 65009 begin
      print 'Die Waffengröße existiert nicht'
    end
    else if error_number() in (65010, 65011) begin
      print 'Das Material wird nicht angeboten'
    end
    else if error_number() = 65012 begin
      print 'Das Setteil existiert nicht'
    end
    else if error_number() = 65013 begin
      print 'Ein Kettenset existiert nicht'
    end
    else begin
      print error_message()
    end
  end catch
end
go
