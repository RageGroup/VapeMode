use [Myrtana]
go

if object_id('P_CreateCourse', 'P') is not null drop procedure P_CreateCourse
go

create procedure P_CreateCourse(
  @CourseName       varchar(80),
  @PricePerTrainer  decimal(6, 2),
  @Duration         int)
as
begin

  begin try
    if exists (select * from Courses where CourseName = @CourseName)
      throw 80000, 'Dieser Kursname ist bereits vergeben.', 1
    if @PricePerTrainer < 0
      throw 80001,'Die Kosten pro Trainer und Kurs liegen unter 0.', 1
    if @Duration < 0
      throw 80002, 'Die Dauer ist negativ.', 1

    insert into Courses (CourseName, PricePerTrainer, Duration)
    values (@CourseName, @PricePerTrainer, @Duration)

    print concat('Der Kurs mit dem Namen ''', @CourseName, ''' wurde erfolgreich angelegt.')
  end try

  begin catch
    if error_number() = 80000 begin
      print concat('Es gibt bereits einen Kurs mit dem Namen ''', @CourseName, '''!')
    end
    else if error_number() = 80001 begin
      print concat('Der Preis pro Trainer liegt bei ', @PricePerTrainer, ' . Es muss ein positiver Wert eingetragen werden!')
    end
    else if error_number() = 80002 begin
      print concat('Die Dauer liegt bei ', @Duration,' . Es muss ein positiver Wert eingetragen werden!')
    end
    else begin
      print error_message()
    end
  end catch

end
go
