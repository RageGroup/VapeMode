use [Myrtana]
go

if object_id('P_DeleteCourse', 'P') is not null drop procedure P_DeleteCourse
go

create procedure P_DeleteCourse(@CourseId int)
as
begin

  begin try
    if not exists (select * from Courses where CourseId = @CourseId)
      throw 80003, 'Diese Kursnummer existiert nicht.', 1

    delete from courses where CourseId = @CourseId

    print concat('Der Kurs mit der Nummer ', @CourseId, ' wurde erfolgreich gelöscht.')
  end try

  begin catch
    if error_number() = 80003 begin
      print concat('Ein Kurs mit der Nummer ', @CourseId, ' existiert nicht!')
    end
    else begin
      print error_message()
    end
  end catch

end
go
