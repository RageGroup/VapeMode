use [Myrtana]
go

if object_id('P_UpdateCourse', 'P') is not null drop procedure P_UpdateCourse
go

create procedure P_UpdateCourse(
  @CourseId         int,
  @CourseName       varchar(80),
  @PricePerTrainer  decimal(6, 2),
  @Duration         int)
as
begin

  begin try
    if not exists (select * from Courses where CourseId = @CourseId)
      throw 80003, 'Diese Kursnummer existiert nicht.', 1
    if @PricePerTrainer < 0
      throw 80001, 'Die Kosten pro Trainer und Kurs liegen unter 0.', 1
    if @Duration < 0
      throw 80002, 'Die Dauer ist negativ.', 1

    update  Courses
    set     CourseName      = coalesce(@CourseName, CourseName),
            PricePerTrainer = coalesce(@PricePerTrainer, PricePerTrainer),
            Duration        = coalesce(@Duration, Duration)
    where   CourseId        = @CourseId

    print concat('Der Kurs mit der Nummer ', @CourseId, ' wurde erfolgreich geändert.')
  end try

  begin catch
    if error_number() = 80001 begin
      print 'Der Preis je Trainer darf nicht negativ sein!'
    end
    else if error_number() = 80002 begin
      print 'Die Dauer des Kurses darf nicht negativ sein!'
    end
    else if error_number() = 80003 begin
      print concat('Ein Kurs mit der Nummer ', @CourseId, ' existiert nicht!')
    end
    else begin
      print error_message()
    end
  end catch

end
go
