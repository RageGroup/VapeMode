use [Myrtana]
go

if object_id('TR_OrderItems', 'TR') is not null drop trigger TR_OrderItems;
go

create trigger TR_OrderItems
on OrderItems
instead of insert, update
as
begin
  declare @Inserting  bit = 0
  declare @Deleting   bit = 0

  if exists (select * from Inserted) set @Inserting = 1
  if exists (select * from Deleted) set @Deleting = 1

  declare @OrderId int

  select @OrderId = OrderId
  from Inserted

  if @Deleting = 0 begin
    declare @OrderLine  int

    select @OrderLine = coalesce(max(OrderLine), 0) + 1
    from OrderItems
    where OrderId = @OrderId

    insert into OrderItems (OrderId, OrderLine, ProductId, Quantity, UnitPrice, IsRepair, CourseId, ParticipantCount)
      select  OrderId, @OrderLine, ProductId, Quantity, UnitPrice, IsRepair, CourseId, ParticipantCount
      from    Inserted
  end
  else begin
    update  OrderItems
    set     ProductId = (select ProductId from Inserted),
            Quantity = (select Quantity from Inserted),
            UnitPrice = (select UnitPrice from Inserted),
            IsRepair = (select IsRepair from Inserted),
            CourseId = (select CourseId from Inserted),
            ParticipantCount = (select ParticipantCount from Inserted)
    where   OrderId = @OrderId
  end
end
go
