use [Myrtana]
go

if object_id('P_NewOrder', 'P') is not null drop procedure [P_NewOrder]
go
create procedure P_NewOrder(
  @CustomerId int)
as
begin

  begin try
    if not exists (select * from Customers where CustomerId = @CustomerId)
      throw 51101, 'Ein Kunde mit diser Kundennummer existiert nicht', 1

    insert into Orders (CustomerId)
    values (@CustomerId)

    print 'Ein Bestellkopf wurde erfolgreich angelegt.'
  end try

  begin catch
    if error_number() = 51101 begin
      print concat('Ein Kunde mit der Kundennummer ', @CustomerId, ' existiert nicht!')
    end
    else begin
      print error_message()
    end
  end catch

end
go
