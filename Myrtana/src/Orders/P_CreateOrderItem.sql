use [Myrtana]
go

if object_id('P_CreateOrderItem', 'P') is not null drop procedure [P_CreateOrderItem]
go
create procedure P_CreateOrderItem(
  @OrderId int,
  @ProductId int = null,
  @Quantity int = null,
  @UnitPrice decimal(8, 2) = null,
  @IsRepair tinyint = null,
  @CourseId int = null,
  @ParticipantCount int = null)
as
begin

  begin try
    if not exists (select * from Orders where OrderId = @OrderId)
      throw 51201, 'Eine Bestellung mit dieser Nummer existiert nicht.', 1

    declare @OrderState char(1)

    select  @OrderState = OrderState
    from    Orders
    where   OrderId = @OrderId

    if @OrderState = 'A'
      throw 51202, 'Die Bestellung ist bereits abgeschlossen.', 1

    if @OrderState = 'U'
      throw 51203, 'Die Bestellung wurde verworfen.', 1

    if @ProductId is not null begin
      if not exists (select * from Products where ProductId = @ProductId)
        throw 51204, 'Ein Artikel mit dieser Artikelnummer existiert nicht.', 1

      -- Bestand abrechnen, den Rest zurückgeben
      declare @Stock int

      update  Products
      set     Stock = Stock - @Quantity
      where   ProductId = @ProductId

      select  @Stock = Stock - @Quantity
      from    Products
      where   ProductId = @ProductId

      -- Wenn nicht genug Bestand, dann Abbruch.
      if @Stock < 0 begin
        update  Products
        set     Stock = Stock + @Quantity
        where   ProductId = @ProductId

        if 1 = 1 throw 51205, 'Vom Artikel mit dieser Nummer ist nicht genug Bestand verfügbar.', 1
      end

      select @UnitPrice = UnitPrice from Products where ProductId = @ProductId

      insert into OrderItems (OrderId, ProductId, Quantity, UnitPrice, IsRepair)
      values (@OrderId, @ProductId, @Quantity, @UnitPrice, @IsRepair)

      update  Orders
      set     Price = Price + (@UnitPrice * @Quantity)
      where   OrderId = @OrderId

      if @IsRepair = 1 begin
        print 'Der Bestellung wurde erfolgreich eine Warenposition hinzugefügt.'
      end
      else begin
        print 'Der Bestellung wurde erfolgreich eine Reparatur hinzugefügt.'
      end
    end
    else if @CourseId is not null begin
      if not exists (select * from Courses where CourseId = @CourseId)
        throw 51206, 'Ein Kurs mit dieser Nummer existiert nicht.', 1

      declare @Price decimal(8, 2)

      select  @Price = PricePerTrainer * ceiling(@ParticipantCount / 12.0)
      from    Courses
      where   CourseId = @CourseId

      insert into OrderItems (OrderId, CourseId, ParticipantCount)
      values (@OrderId, @CourseId, @ParticipantCount)

      update  Orders
      set     Price = Price + @Price
      where   OrderId = @OrderId

      print 'Der Bestellung wurde erfolgreich eine Gruppenkursbuchung hinzugefügt.'
    end
    else
      throw 51207, 'Für die Erstellung einer Bestellposition muss entweder eine Produktnummer oder eine Kursnummer übergeben werden', 1
  end try

  begin catch
    if error_number() = 51201 begin
      print concat('Eine Bestellung mit der Nummer ', @OrderId, ' existiert nicht!')
    end
    else if error_number() = 51202 begin
      print concat('Die Bestellung mit der Nummer ', @OrderId, ' ist bereits abgeschlossen!')
    end
    else if error_number() = 51203 begin
      print concat('Die Bestellung mit der Nummer ', @OrderId, ' wurde verworfen.!')
    end
    else if error_number() = 51205 begin
      print concat('Vom Artikel mit der Nummer ', @ProductId, ' sind ', -@Stock, ' Stück zu wenig verfügbar!')
    end
    else if error_number() = 51206 begin
      print concat('Ein Kurs mit der Nummer ', @CourseId, ' existiert nicht!')
    end
    else if error_number() = 51207 begin
      print 'Für die Erstellung einer Bestellposition muss entweder eine Produktnummer oder eine Kursnummer übergeben werden!'
    end
    else begin
      print error_message()
    end
  end catch

end
go
