use [Myrtana]
go

if object_id('P_SetOrderState', 'P') is not null drop procedure [P_SetOrderState]
go
create procedure P_SetOrderState(
  @OrderId int,
  @OrderState char(1))
as
begin
  begin try
    if not exists (select * from Orders where OrderId = @OrderId)
      throw 51301, 'Eine Bestellung mit dieser Nummer existiert nicht.', 1

    declare @OldOrderState char(1)

    select  @OldOrderState = OrderState
    from    Orders
    where   OrderId    = @OrderId

    if @OldOrderState = 'A'
      throw 51302, 'Diese Bestellung ist bereits abgeschlossen.', 1

    update  Orders
    set     OrderState = @OrderState
    where   OrderId    = @OrderId
  end try
  begin catch
    if error_number() = 51301 begin
      print concat('Eine Bestellung mit der Nummer ', @OrderId, ' existiert nicht!')
    end
    else if error_number() = 51302 begin
      print concat('Diese Bestellung mit der Nummer ', @OrderId, ' ist bereits abgeschlossen!')
    end
    else begin
      print error_message()
    end
  end catch
end
go
