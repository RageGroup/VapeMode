/*
begin transaction
--============================================================================--

----------------------------------------
execute P_NewOrder @CustomerId = 1
go

----------------------------------------
execute P_CreateOrderItem 1, @ProductId = 1, @Quantity = 13, @IsRepair = 0

----------------------------------------
execute P_CreateOrderItem 1, @ProductId = 1, @Quantity = 7, @IsRepair = 1

execute P_CreateOrderItem 1, @CourseId = 1, @ParticipantCount = 15
go

----------------------------------------
execute P_SetOrderState 1, 'A'
go

--============================================================================--
rollback transaction
go
--*/
