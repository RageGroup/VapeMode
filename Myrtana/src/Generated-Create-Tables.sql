﻿use [Myrtana]
go

if object_id('Employees', 'U') is not null drop table [Employees]
go

create table [Employees](
  EmployeeId    int          not null identity(1, 1),
  FirstName     varchar(50)  not null,
  LastName      varchar(50)  not null,
  Postcode      char(5)      not null,
  City          varchar(50)  not null,
  StreetHouseNo varchar(80)  not null,
  HireDate      date         not null default getdate(),
  Salary        decimal(6,2) not null,
  Manager       int          null,
  EmployeeType  char(1)      not null)
go

execute sys.sp_addextendedproperty 'MS_Description', 'Mitarbeiter' , 'SCHEMA', 'dbo', 'TABLE', 'Employees'
go

execute sys.sp_addextendedproperty 'MS_Description', 'Mitarbeiternummer', 'SCHEMA', 'dbo', 'TABLE', 'Employees', 'COLUMN', 'EmployeeId'
execute sys.sp_addextendedproperty 'MS_Description', 'Vorname', 'SCHEMA', 'dbo', 'TABLE', 'Employees', 'COLUMN', 'FirstName'
execute sys.sp_addextendedproperty 'MS_Description', 'Nachname', 'SCHEMA', 'dbo', 'TABLE', 'Employees', 'COLUMN', 'LastName'
execute sys.sp_addextendedproperty 'MS_Description', 'Postleitzahl', 'SCHEMA', 'dbo', 'TABLE', 'Employees', 'COLUMN', 'Postcode'
execute sys.sp_addextendedproperty 'MS_Description', 'Ort', 'SCHEMA', 'dbo', 'TABLE', 'Employees', 'COLUMN', 'City'
execute sys.sp_addextendedproperty 'MS_Description', 'Straße und Hausnummer', 'SCHEMA', 'dbo', 'TABLE', 'Employees', 'COLUMN', 'StreetHouseNo'
execute sys.sp_addextendedproperty 'MS_Description', 'Einstellungsdatum', 'SCHEMA', 'dbo', 'TABLE', 'Employees', 'COLUMN', 'HireDate'
execute sys.sp_addextendedproperty 'MS_Description', 'Gehalt', 'SCHEMA', 'dbo', 'TABLE', 'Employees', 'COLUMN', 'Salary'
execute sys.sp_addextendedproperty 'MS_Description', 'Vorgesetzter', 'SCHEMA', 'dbo', 'TABLE', 'Employees', 'COLUMN', 'Manager'
execute sys.sp_addextendedproperty 'MS_Description', 'Mitarbeitertyp (M => Manager, V => Verkäufer, H => Handwerker, B => Büroangestellter, T => Trainer)', 'SCHEMA', 'dbo', 'TABLE', 'Employees', 'COLUMN', 'EmployeeType'
go

if object_id('Customers', 'U') is not null drop table [Customers]
go

create table [Customers](
  CustomerId    int         not null identity(1, 1),
  CustomerName  varchar(80) not null,
  Contact       varchar(80) not null,
  Postcode      char(5)     not null,
  City          varchar(50) not null,
  StreetHouseNo varchar(80) not null)
go

execute sys.sp_addextendedproperty 'MS_Description', 'Kunden' , 'SCHEMA', 'dbo', 'TABLE', 'Customers'
go

execute sys.sp_addextendedproperty 'MS_Description', 'Kundennummer', 'SCHEMA', 'dbo', 'TABLE', 'Customers', 'COLUMN', 'CustomerId'
execute sys.sp_addextendedproperty 'MS_Description', 'Name der Organisation', 'SCHEMA', 'dbo', 'TABLE', 'Customers', 'COLUMN', 'CustomerName'
execute sys.sp_addextendedproperty 'MS_Description', 'Ansprechpartner', 'SCHEMA', 'dbo', 'TABLE', 'Customers', 'COLUMN', 'Contact'
execute sys.sp_addextendedproperty 'MS_Description', 'Postleitzahl', 'SCHEMA', 'dbo', 'TABLE', 'Customers', 'COLUMN', 'Postcode'
execute sys.sp_addextendedproperty 'MS_Description', 'Ort', 'SCHEMA', 'dbo', 'TABLE', 'Customers', 'COLUMN', 'City'
execute sys.sp_addextendedproperty 'MS_Description', 'Straße und Hausnummer', 'SCHEMA', 'dbo', 'TABLE', 'Customers', 'COLUMN', 'StreetHouseNo'
go

if object_id('Products', 'U') is not null drop table [Products]
go

create table [Products](
  ProductId       int           not null identity(1, 1),
  ProductName     varchar(80)   not null,
  Material        char(2)       not null,
  Stock           int           not null,
  ProductCategory char(1)       not null,
  UnitPrice       decimal(8, 2) not null,
  WeaponType      char(2)       null,
  WeaponSize      char(1)       null,
  ArmorPart       char(2)       null)
go

execute sys.sp_addextendedproperty 'MS_Description', 'Produkte' , 'SCHEMA', 'dbo', 'TABLE', 'Products'
go

execute sys.sp_addextendedproperty 'MS_Description', 'Produktnummer', 'SCHEMA', 'dbo', 'TABLE', 'Products', 'COLUMN', 'ProductId'
execute sys.sp_addextendedproperty 'MS_Description', 'Produktname', 'SCHEMA', 'dbo', 'TABLE', 'Products', 'COLUMN', 'ProductName'
execute sys.sp_addextendedproperty 'MS_Description', 'Material (Ho => Holz, Me => Metall, St => Stoff, Le => Leder, Ke => Ketten, Pl => Platten)', 'SCHEMA', 'dbo', 'TABLE', 'Products', 'COLUMN', 'Material'
execute sys.sp_addextendedproperty 'MS_Description', 'Bestand', 'SCHEMA', 'dbo', 'TABLE', 'Products', 'COLUMN', 'Stock'
execute sys.sp_addextendedproperty 'MS_Description', 'Produktkategorie (W => Waffen, R => Rüstungen, P => Pferdezubehör)', 'SCHEMA', 'dbo', 'TABLE', 'Products', 'COLUMN', 'ProductCategory'
execute sys.sp_addextendedproperty 'MS_Description', 'Stückpreis', 'SCHEMA', 'dbo', 'TABLE', 'Products', 'COLUMN', 'UnitPrice'
execute sys.sp_addextendedproperty 'MS_Description', 'Waffentyp (Do => Dolch, Ax => Axt, Sc => Schwert, La => Langschwert, Bo => Bogen, Ar => Armbrust)', 'SCHEMA', 'dbo', 'TABLE', 'Products', 'COLUMN', 'WeaponType'
execute sys.sp_addextendedproperty 'MS_Description', 'Waffengröße', 'SCHEMA', 'dbo', 'TABLE', 'Products', 'COLUMN', 'WeaponSize'
execute sys.sp_addextendedproperty 'MS_Description', 'Rüstungsteil/Set (He => Helm, Br => Brustrüstung, Ha => Handschuh, Be => Beinrüstung, Sc => Schuh, Se => Set)', 'SCHEMA', 'dbo', 'TABLE', 'Products', 'COLUMN', 'ArmorPart'
go

if object_id('Courses', 'U') is not null drop table [Courses]
go

create table [Courses](
  CourseId        int           not null identity(1, 1),
  CourseName      varchar(80)   not null,
  PricePerTrainer decimal(6, 2) not null,
  Duration        int           not null)
go

execute sys.sp_addextendedproperty 'MS_Description', 'Gruppenkurse' , 'SCHEMA', 'dbo', 'TABLE', 'Courses'
go

execute sys.sp_addextendedproperty 'MS_Description', 'Kursnummer', 'SCHEMA', 'dbo', 'TABLE', 'Courses', 'COLUMN', 'CourseId'
execute sys.sp_addextendedproperty 'MS_Description', 'Kursname', 'SCHEMA', 'dbo', 'TABLE', 'Courses', 'COLUMN', 'CourseName'
execute sys.sp_addextendedproperty 'MS_Description', 'Preis je Trainer', 'SCHEMA', 'dbo', 'TABLE', 'Courses', 'COLUMN', 'PricePerTrainer'
execute sys.sp_addextendedproperty 'MS_Description', 'Dauer', 'SCHEMA', 'dbo', 'TABLE', 'Courses', 'COLUMN', 'Duration'
go

if object_id('Orders', 'U') is not null drop table [Orders]
go

create table [Orders](
  OrderId    int           not null identity(1, 1),
  OrderDate  date          not null default getdate(),
  OrderState char(1)       not null default 'O',
  CustomerId int           not null,
  Price      decimal(8, 2) not null default 0)
go

execute sys.sp_addextendedproperty 'MS_Description', 'Bestellungen' , 'SCHEMA', 'dbo', 'TABLE', 'Orders'
go

execute sys.sp_addextendedproperty 'MS_Description', 'Bestellnummer', 'SCHEMA', 'dbo', 'TABLE', 'Orders', 'COLUMN', 'OrderId'
execute sys.sp_addextendedproperty 'MS_Description', 'Bestelldatum', 'SCHEMA', 'dbo', 'TABLE', 'Orders', 'COLUMN', 'OrderDate'
execute sys.sp_addextendedproperty 'MS_Description', 'Bestellstatus (''O'' => Offen, ''A'' => Abgeschlossen, ''U'' => Ungültig)', 'SCHEMA', 'dbo', 'TABLE', 'Orders', 'COLUMN', 'OrderState'
execute sys.sp_addextendedproperty 'MS_Description', 'Kundennummer', 'SCHEMA', 'dbo', 'TABLE', 'Orders', 'COLUMN', 'CustomerId'
execute sys.sp_addextendedproperty 'MS_Description', 'Gesamtpreis', 'SCHEMA', 'dbo', 'TABLE', 'Orders', 'COLUMN', 'Price'
go

if object_id('OrderItems', 'U') is not null drop table [OrderItems]
go

create table [OrderItems](
  OrderId          int           not null,
  OrderLine        int           not null,
  ProductId        int           null,
  Quantity         int           null,
  UnitPrice        decimal(8, 2) null,
  IsRepair         tinyint       null,
  CourseId         int           null,
  ParticipantCount int           null)
go

execute sys.sp_addextendedproperty 'MS_Description', 'Bestellpositionen' , 'SCHEMA', 'dbo', 'TABLE', 'OrderItems'
go

execute sys.sp_addextendedproperty 'MS_Description', 'Bestellnummer', 'SCHEMA', 'dbo', 'TABLE', 'OrderItems', 'COLUMN', 'OrderId'
execute sys.sp_addextendedproperty 'MS_Description', 'Bestellpositionsnummer', 'SCHEMA', 'dbo', 'TABLE', 'OrderItems', 'COLUMN', 'OrderLine'
execute sys.sp_addextendedproperty 'MS_Description', 'Produktnummer', 'SCHEMA', 'dbo', 'TABLE', 'OrderItems', 'COLUMN', 'ProductId'
execute sys.sp_addextendedproperty 'MS_Description', 'Bestellmenge', 'SCHEMA', 'dbo', 'TABLE', 'OrderItems', 'COLUMN', 'Quantity'
execute sys.sp_addextendedproperty 'MS_Description', 'Stückpreis', 'SCHEMA', 'dbo', 'TABLE', 'OrderItems', 'COLUMN', 'UnitPrice'
execute sys.sp_addextendedproperty 'MS_Description', 'Reparatur? (Ansonsten Kauf)', 'SCHEMA', 'dbo', 'TABLE', 'OrderItems', 'COLUMN', 'IsRepair'
execute sys.sp_addextendedproperty 'MS_Description', 'Gruppenkursnummer', 'SCHEMA', 'dbo', 'TABLE', 'OrderItems', 'COLUMN', 'CourseId'
execute sys.sp_addextendedproperty 'MS_Description', 'Teilnehmeranzahl', 'SCHEMA', 'dbo', 'TABLE', 'OrderItems', 'COLUMN', 'ParticipantCount'
go

/*
alter table Employees drop constraint FK_Employees_Employees
go
alter table Orders drop constraint FK_Orders_Customers
go
alter table OrderItems drop constraint FK_OrderItems_Orders
go
alter table OrderItems drop constraint FK_OrderItems_Products
go
alter table OrderItems drop constraint FK_OrderItems_Courses
go
--*/

alter table [Employees]
add
  constraint PK_Employees primary key (EmployeeId),
  constraint FK_Employees_Employees foreign key (Manager) references [Employees] (EmployeeId),
  constraint CK_EmployeeType check (EmployeeType in ('M', 'V', 'H', 'B', 'T'))
go

alter table [Customers]
add
  constraint PK_Customers primary key (CustomerId)
go

alter table [Products]
add
  constraint PK_Products primary key (ProductId),
  constraint CK_Material check ((ProductCategory <> 'W' and Material in ('St', 'Le', 'Ke', 'Pl')) or (ProductCategory = 'W' and Material in ('Ho', 'Me'))),
  constraint CK_ProductCategory check (ProductCategory in ('W', 'R', 'P')),
  constraint CK_WeaponType check (ProductCategory <> 'W' or (ProductCategory = 'W' and WeaponType in ('Do', 'Ax', 'Sc', 'La', 'Bo', 'Ar'))),
  constraint CK_WeaponSize check (ProductCategory <> 'W' or (ProductCategory = 'W' and WeaponSize in ('S', 'M', 'L'))),
  constraint CK_ArmorPart check (ProductCategory <> 'R' or (ProductCategory = 'R' and ArmorPart in ('He', 'Br', 'Ha', 'Be', 'Sc', 'Se')))
go

alter table [Courses]
add
  constraint PK_Courses primary key (CourseId)
go

alter table [Orders]
add
  constraint PK_Orders primary key (OrderId),
  constraint FK_Orders_Customers foreign key (CustomerId) references [Customers] (CustomerId),
  constraint CK_OrderState check (OrderState in ('O', 'A', 'U'))
go

alter table [OrderItems]
add
  constraint PK_OrderItems primary key (OrderId, OrderLine),
  constraint FK_OrderItems_Orders foreign key (OrderId) references [Orders] (OrderId),
  constraint FK_OrderItems_Products foreign key (ProductId) references [Products] (ProductId),
  constraint FK_OrderItems_Courses foreign key (CourseId) references [Courses] (CourseId)
go

