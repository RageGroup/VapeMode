﻿use [Myrtana]
go

if object_id('P_Employees_Insert', 'P') is not null drop procedure [P_Employees_Insert]
go
create procedure P_Employees_Insert(
  @EmployeeId int,
  @FirstName varchar(50),
  @LastName varchar(50),
  @Postcode char(5),
  @City varchar(50),
  @StreetHouseNo varchar(80),
  @HireDate date,
  @Salary decimal(6,2),
  @Manager int,
  @EmployeeType char(1))
as
begin
  begin try
    insert into Employees(
      EmployeeId,
      FirstName,
      LastName,
      Postcode,
      City,
      StreetHouseNo,
      HireDate,
      Salary,
      Manager,
      EmployeeType)
    values(
      @EmployeeId,
      @FirstName,
      @LastName,
      @Postcode,
      @City,
      @StreetHouseNo,
      @HireDate,
      @Salary,
      @Manager,
      @EmployeeType)
  end try
  begin catch
    declare @errno int
    declare @errmsg varchar(256)
    select  @errno  = error_number(),
            @errmsg = error_message()
    print concat('Fehler (', @errno, '): ', @errmsg)
  end catch
end
go

if object_id('P_Employees_Update', 'P') is not null drop procedure [P_Employees_Update]
go
create procedure P_Employees_Update(
  @EmployeeId int,
  @FirstName varchar(50) = null,
  @LastName varchar(50) = null,
  @Postcode char(5) = null,
  @City varchar(50) = null,
  @StreetHouseNo varchar(80) = null,
  @HireDate date = null,
  @Salary decimal(6,2) = null,
  @Manager int = null,
  @EmployeeType char(1) = null)
as
begin
  begin try
    update Employees
    set   FirstName = coalesce(@FirstName, FirstName),
          LastName = coalesce(@LastName, LastName),
          Postcode = coalesce(@Postcode, Postcode),
          City = coalesce(@City, City),
          StreetHouseNo = coalesce(@StreetHouseNo, StreetHouseNo),
          HireDate = coalesce(@HireDate, HireDate),
          Salary = coalesce(@Salary, Salary),
          Manager = coalesce(@Manager, Manager),
          EmployeeType = coalesce(@EmployeeType, EmployeeType)
    where EmployeeId = @EmployeeId
  end try
  begin catch
    declare @errno int
    declare @errmsg varchar(256)
    select  @errno  = error_number(),
            @errmsg = error_message()
    print concat('Fehler (', @errno, '): ', @errmsg)
  end catch
end
go

if object_id('P_Employees_Delete', 'P') is not null drop procedure [P_Employees_Delete]
go
create procedure P_Employees_Delete(
  @EmployeeId int)
as
begin
  begin try
    delete from Employees
    where EmployeeId = @EmployeeId
  end try
  begin catch
    declare @errno int
    declare @errmsg varchar(256)
    select  @errno  = error_number(),
            @errmsg = error_message()
    print concat('Fehler (', @errno, '): ', @errmsg)
  end catch
end
go

if object_id('P_Customers_Insert', 'P') is not null drop procedure [P_Customers_Insert]
go
create procedure P_Customers_Insert(
  @CustomerId int,
  @CustomerName varchar(80),
  @Contact varchar(80),
  @Postcode char(5),
  @City varchar(50),
  @StreetHouseNo varchar(80))
as
begin
  begin try
    insert into Customers(
      CustomerId,
      CustomerName,
      Contact,
      Postcode,
      City,
      StreetHouseNo)
    values(
      @CustomerId,
      @CustomerName,
      @Contact,
      @Postcode,
      @City,
      @StreetHouseNo)
  end try
  begin catch
    declare @errno int
    declare @errmsg varchar(256)
    select  @errno  = error_number(),
            @errmsg = error_message()
    print concat('Fehler (', @errno, '): ', @errmsg)
  end catch
end
go

if object_id('P_Customers_Update', 'P') is not null drop procedure [P_Customers_Update]
go
create procedure P_Customers_Update(
  @CustomerId int,
  @CustomerName varchar(80) = null,
  @Contact varchar(80) = null,
  @Postcode char(5) = null,
  @City varchar(50) = null,
  @StreetHouseNo varchar(80) = null)
as
begin
  begin try
    update Customers
    set   CustomerName = coalesce(@CustomerName, CustomerName),
          Contact = coalesce(@Contact, Contact),
          Postcode = coalesce(@Postcode, Postcode),
          City = coalesce(@City, City),
          StreetHouseNo = coalesce(@StreetHouseNo, StreetHouseNo)
    where CustomerId = @CustomerId
  end try
  begin catch
    declare @errno int
    declare @errmsg varchar(256)
    select  @errno  = error_number(),
            @errmsg = error_message()
    print concat('Fehler (', @errno, '): ', @errmsg)
  end catch
end
go

if object_id('P_Customers_Delete', 'P') is not null drop procedure [P_Customers_Delete]
go
create procedure P_Customers_Delete(
  @CustomerId int)
as
begin
  begin try
    delete from Customers
    where CustomerId = @CustomerId
  end try
  begin catch
    declare @errno int
    declare @errmsg varchar(256)
    select  @errno  = error_number(),
            @errmsg = error_message()
    print concat('Fehler (', @errno, '): ', @errmsg)
  end catch
end
go

if object_id('P_Products_Insert', 'P') is not null drop procedure [P_Products_Insert]
go
create procedure P_Products_Insert(
  @ProductId int,
  @ProductName varchar(80),
  @Material char(2),
  @Stock int,
  @ProductCategory char(1),
  @UnitPrice decimal(8, 2),
  @WeaponType char(2),
  @WeaponSize char(1),
  @ArmorPart char(2))
as
begin
  begin try
    insert into Products(
      ProductId,
      ProductName,
      Material,
      Stock,
      ProductCategory,
      UnitPrice,
      WeaponType,
      WeaponSize,
      ArmorPart)
    values(
      @ProductId,
      @ProductName,
      @Material,
      @Stock,
      @ProductCategory,
      @UnitPrice,
      @WeaponType,
      @WeaponSize,
      @ArmorPart)
  end try
  begin catch
    declare @errno int
    declare @errmsg varchar(256)
    select  @errno  = error_number(),
            @errmsg = error_message()
    print concat('Fehler (', @errno, '): ', @errmsg)
  end catch
end
go

if object_id('P_Products_Update', 'P') is not null drop procedure [P_Products_Update]
go
create procedure P_Products_Update(
  @ProductId int,
  @ProductName varchar(80) = null,
  @Material char(2) = null,
  @Stock int = null,
  @ProductCategory char(1) = null,
  @UnitPrice decimal(8, 2) = null,
  @WeaponType char(2) = null,
  @WeaponSize char(1) = null,
  @ArmorPart char(2) = null)
as
begin
  begin try
    update Products
    set   ProductName = coalesce(@ProductName, ProductName),
          Material = coalesce(@Material, Material),
          Stock = coalesce(@Stock, Stock),
          ProductCategory = coalesce(@ProductCategory, ProductCategory),
          UnitPrice = coalesce(@UnitPrice, UnitPrice),
          WeaponType = coalesce(@WeaponType, WeaponType),
          WeaponSize = coalesce(@WeaponSize, WeaponSize),
          ArmorPart = coalesce(@ArmorPart, ArmorPart)
    where ProductId = @ProductId
  end try
  begin catch
    declare @errno int
    declare @errmsg varchar(256)
    select  @errno  = error_number(),
            @errmsg = error_message()
    print concat('Fehler (', @errno, '): ', @errmsg)
  end catch
end
go

if object_id('P_Products_Delete', 'P') is not null drop procedure [P_Products_Delete]
go
create procedure P_Products_Delete(
  @ProductId int)
as
begin
  begin try
    delete from Products
    where ProductId = @ProductId
  end try
  begin catch
    declare @errno int
    declare @errmsg varchar(256)
    select  @errno  = error_number(),
            @errmsg = error_message()
    print concat('Fehler (', @errno, '): ', @errmsg)
  end catch
end
go

if object_id('P_Courses_Insert', 'P') is not null drop procedure [P_Courses_Insert]
go
create procedure P_Courses_Insert(
  @CourseId int,
  @CourseName varchar(80),
  @PricePerTrainer decimal(6, 2),
  @Duration int)
as
begin
  begin try
    insert into Courses(
      CourseId,
      CourseName,
      PricePerTrainer,
      Duration)
    values(
      @CourseId,
      @CourseName,
      @PricePerTrainer,
      @Duration)
  end try
  begin catch
    declare @errno int
    declare @errmsg varchar(256)
    select  @errno  = error_number(),
            @errmsg = error_message()
    print concat('Fehler (', @errno, '): ', @errmsg)
  end catch
end
go

if object_id('P_Courses_Update', 'P') is not null drop procedure [P_Courses_Update]
go
create procedure P_Courses_Update(
  @CourseId int,
  @CourseName varchar(80) = null,
  @PricePerTrainer decimal(6, 2) = null,
  @Duration int = null)
as
begin
  begin try
    update Courses
    set   CourseName = coalesce(@CourseName, CourseName),
          PricePerTrainer = coalesce(@PricePerTrainer, PricePerTrainer),
          Duration = coalesce(@Duration, Duration)
    where CourseId = @CourseId
  end try
  begin catch
    declare @errno int
    declare @errmsg varchar(256)
    select  @errno  = error_number(),
            @errmsg = error_message()
    print concat('Fehler (', @errno, '): ', @errmsg)
  end catch
end
go

if object_id('P_Courses_Delete', 'P') is not null drop procedure [P_Courses_Delete]
go
create procedure P_Courses_Delete(
  @CourseId int)
as
begin
  begin try
    delete from Courses
    where CourseId = @CourseId
  end try
  begin catch
    declare @errno int
    declare @errmsg varchar(256)
    select  @errno  = error_number(),
            @errmsg = error_message()
    print concat('Fehler (', @errno, '): ', @errmsg)
  end catch
end
go

if object_id('P_Orders_Insert', 'P') is not null drop procedure [P_Orders_Insert]
go
create procedure P_Orders_Insert(
  @OrderId int,
  @OrderDate date,
  @OrderState char(1),
  @CustomerId int,
  @Price decimal(8, 2))
as
begin
  begin try
    insert into Orders(
      OrderId,
      OrderDate,
      OrderState,
      CustomerId,
      Price)
    values(
      @OrderId,
      @OrderDate,
      @OrderState,
      @CustomerId,
      @Price)
  end try
  begin catch
    declare @errno int
    declare @errmsg varchar(256)
    select  @errno  = error_number(),
            @errmsg = error_message()
    print concat('Fehler (', @errno, '): ', @errmsg)
  end catch
end
go

if object_id('P_Orders_Update', 'P') is not null drop procedure [P_Orders_Update]
go
create procedure P_Orders_Update(
  @OrderId int,
  @OrderDate date = null,
  @OrderState char(1) = null,
  @CustomerId int = null,
  @Price decimal(8, 2) = null)
as
begin
  begin try
    update Orders
    set   OrderDate = coalesce(@OrderDate, OrderDate),
          OrderState = coalesce(@OrderState, OrderState),
          CustomerId = coalesce(@CustomerId, CustomerId),
          Price = coalesce(@Price, Price)
    where OrderId = @OrderId
  end try
  begin catch
    declare @errno int
    declare @errmsg varchar(256)
    select  @errno  = error_number(),
            @errmsg = error_message()
    print concat('Fehler (', @errno, '): ', @errmsg)
  end catch
end
go

if object_id('P_Orders_Delete', 'P') is not null drop procedure [P_Orders_Delete]
go
create procedure P_Orders_Delete(
  @OrderId int)
as
begin
  begin try
    delete from Orders
    where OrderId = @OrderId
  end try
  begin catch
    declare @errno int
    declare @errmsg varchar(256)
    select  @errno  = error_number(),
            @errmsg = error_message()
    print concat('Fehler (', @errno, '): ', @errmsg)
  end catch
end
go

if object_id('P_OrderItems_Insert', 'P') is not null drop procedure [P_OrderItems_Insert]
go
create procedure P_OrderItems_Insert(
  @OrderId int,
  @OrderLine int,
  @ProductId int,
  @Quantity int,
  @UnitPrice decimal(8, 2),
  @IsRepair tinyint,
  @CourseId int,
  @ParticipantCount int)
as
begin
  begin try
    insert into OrderItems(
      OrderId,
      OrderLine,
      ProductId,
      Quantity,
      UnitPrice,
      IsRepair,
      CourseId,
      ParticipantCount)
    values(
      @OrderId,
      @OrderLine,
      @ProductId,
      @Quantity,
      @UnitPrice,
      @IsRepair,
      @CourseId,
      @ParticipantCount)
  end try
  begin catch
    declare @errno int
    declare @errmsg varchar(256)
    select  @errno  = error_number(),
            @errmsg = error_message()
    print concat('Fehler (', @errno, '): ', @errmsg)
  end catch
end
go

if object_id('P_OrderItems_Update', 'P') is not null drop procedure [P_OrderItems_Update]
go
create procedure P_OrderItems_Update(
  @OrderId int,
  @OrderLine int,
  @ProductId int = null,
  @Quantity int = null,
  @UnitPrice decimal(8, 2) = null,
  @IsRepair tinyint = null,
  @CourseId int = null,
  @ParticipantCount int = null)
as
begin
  begin try
    update OrderItems
    set   ProductId = coalesce(@ProductId, ProductId),
          Quantity = coalesce(@Quantity, Quantity),
          UnitPrice = coalesce(@UnitPrice, UnitPrice),
          IsRepair = coalesce(@IsRepair, IsRepair),
          CourseId = coalesce(@CourseId, CourseId),
          ParticipantCount = coalesce(@ParticipantCount, ParticipantCount)
    where OrderId = @OrderId and
          OrderLine = @OrderLine
  end try
  begin catch
    declare @errno int
    declare @errmsg varchar(256)
    select  @errno  = error_number(),
            @errmsg = error_message()
    print concat('Fehler (', @errno, '): ', @errmsg)
  end catch
end
go

if object_id('P_OrderItems_Delete', 'P') is not null drop procedure [P_OrderItems_Delete]
go
create procedure P_OrderItems_Delete(
  @OrderId int,
  @OrderLine int)
as
begin
  begin try
    delete from OrderItems
    where OrderId = @OrderId and
          OrderLine = @OrderLine
  end try
  begin catch
    declare @errno int
    declare @errmsg varchar(256)
    select  @errno  = error_number(),
            @errmsg = error_message()
    print concat('Fehler (', @errno, '): ', @errmsg)
  end catch
end
go

