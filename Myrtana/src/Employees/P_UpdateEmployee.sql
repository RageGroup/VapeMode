use [Myrtana]
go

if object_id('P_UpdateEmployee', 'P') is not null drop procedure [P_UpdateEmployee]
go

create procedure P_UpdateEmployee(
  @EmployeeId     int,
  @FirstName      varchar(50)   = null,
  @LastName       varchar(50)   = null,
  @Postcode       varchar(5)    = null,
  @City           varchar(50)   = null,
  @StreetHouseNo  varchar(80)   = null,
  @Salary         decimal(6, 2) = null,
  @Manager        int           = null,
  @EmployeeType   char(1)       = null)
as
begin
  begin try

    if not exists (select * from Employees where @EmployeeId = EmployeeId)
      throw 55005, 'Der Mitarbeiter existiert nicht.', 1
    if @EmployeeType not in ('V', 'H', 'B', 'T', 'M')
      throw 55006, 'Dieser Mitarbeitertyp ist nich gestattet.', 1
    if @EmployeeType = 'M' begin
      set @Manager = null
    end
    if @Manager is not null begin
      if not exists (select * from Employees where @Manager = Manager)
        throw 55007, 'Der gewählte Manager existiert nicht.', 1
    end
    if @Manager is null and @EmployeeType = 'M' begin
      update  Employees
      set     Manager = @Manager
      where   EmployeeId = @EmployeeId
    end
    else begin
      update  Employees
      set     Manager = coalesce(@Manager, Manager)
      where   EmployeeId = @EmployeeId
    end

    update  Employees
    set     FirstName     = coalesce(@FirstName, FirstName),
            LastName      = coalesce(@LastName, LastName),
            Postcode      = coalesce(@Postcode, PostCode),
            City          = coalesce(@City, City),
            StreetHouseNo = coalesce(@StreetHouseNo, StreetHouseNo),
            Salary        = coalesce(@Salary, Salary),
            EmployeeType  = coalesce(@EmployeeType, EmployeeType)
    where   EmployeeId    = @EmployeeId

    print concat('Die Daten für den Mitarbeiter ', @LastName, ', ' , @FirstName, ' wurden erfolgreich editiert.')
  end try

  begin catch
    if error_number() = 55005 begin
      print 'Der Mitarbeiter existiert nicht.'
    end
    else if error_number() = 55006 begin
      print 'Dieser Mitarbeitertyp ist nich gestattet.'
    end
    else if error_number() = 55007 begin
      print 'Der gewählte Manager existiert nicht.'
    end
    else begin
      print error_message()
    end
  end catch

end
go
