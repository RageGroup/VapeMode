use [Myrtana]
go

if object_id('P_CreateEmployee', 'P') is not null drop procedure [P_CreateEmployee]
go

create procedure P_CreateEmployee(
  @FirstName      varchar(50),
  @LastName       varchar(50),
  @PostCode       varchar(5),
  @City           varchar(50),
  @StreetHouseNo  varchar(80),
  @Salary         decimal(6,2),
  @Manager        int,
  @EmployeeType   char(1))
as
begin

  begin try
    if exists (select * from Employees where @FirstName = FirstName and @LastName = LastName)
      throw 55000, 'Ein Mitarbeiter mit diesem Namen existiert bereits.', 1
    if @EmployeeType not in ('V', 'H', 'B', 'T', 'M')
      throw 55003, 'Der angegebene Mitarbeitertyp ist unbekannt.', 1

    if @Manager is not null begin
      if not exists (select * from Employees where EmployeeId = @Manager and Manager is null)
        throw 55001, 'Der Manager mit dieser Nummer existiert nicht.', 1
    end
    if exists (select * from Employees where @Manager = Manager and @EmployeeType = 'M') throw 55002, '', 1

    insert into Employees (FirstName, LastName, Postcode, City, StreetHouseNo, Salary, Manager, EmployeeType)
    values (@FirstName, @LastName, @Postcode, @City, @StreetHouseNo, @Salary, @Manager, @EmployeeType)

    print concat('Der Mitarbeiter mit dem Namen ', @LastName, ', ', @FirstName, ' wurde erfolgreich in der Datenbank angelegt.')
  end try

  begin catch
    if error_number() = 55000 begin
      print concat('Der Mitarbeiter mit dem Namen ', @LastName, ', s', @FirstName,  ' existiert bereits!')
    end
    else if error_number() = 55001 begin
      print concat('Der Manager mit der Nummer ', @Manager, ' existiert nicht!')
    end
    else if error_number() = 55002 begin
      print 'Manager können keine Manager haben.'
    end
    else if error_number() = 55003 begin
      print concat('Der Mitarbeitertyp ''', @EmployeeType, '''ist unbekannt!')
    end
    else begin
      print error_message()
    end
  end catch

end
go
