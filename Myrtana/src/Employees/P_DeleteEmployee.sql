use [Myrtana]
go

if object_id('P_DeleteEmployee', 'P') is not null drop procedure [P_DeleteEmployee]
go

create procedure P_DeleteEmployee(@EmployeeId int)
as
begin

  begin try
    if not exists (select * from Employees where @EmployeeId = EmployeeId)
      throw 55004, 'Ein Mitarbeiter mit dieser Nummer existiert nicht.', 1
     
     delete from Employees
     where EmployeeId = @EmployeeId

     print 'Der Mitarbeiter wurde erfolgreich gelöscht.'
  end try


  begin catch
    if error_number() = 55004 begin
      print concat('Der Mitarbeiter ', @EmployeeId, ' existiert nicht.')
    end
    else begin
      print error_message()
    end
  end catch

end
go
