/*******************************************************************************
* 1.  Erstellung der Datenbank
*******************************************************************************/

--/*
use master
go

execute msdb.dbo.sp_delete_database_backuphistory @database_name = 'Myrtana'
go

if exists (select * from sys.databases where name = 'Myrtana') drop database [Myrtana]
go

create database [Myrtana]
go
--/*

/*******************************************************************************
* 2.  Erstellung der Tabellen
*******************************************************************************/
use [Myrtana]
go

if object_id('Employees', 'U') is not null drop table [Employees]
go

create table [Employees](
  EmployeeId    int          not null identity(1, 1),
  FirstName     varchar(50)  not null,
  LastName      varchar(50)  not null,
  Postcode      char(5)      not null,
  City          varchar(50)  not null,
  StreetHouseNo varchar(80)  not null,
  HireDate      date         not null default getdate(),
  Salary        decimal(6,2) not null,
  Manager       int          null,
  EmployeeType  char(1)      not null)
go

execute sys.sp_addextendedproperty 'MS_Description', 'Mitarbeiter' , 'SCHEMA', 'dbo', 'TABLE', 'Employees'
go

execute sys.sp_addextendedproperty 'MS_Description', 'Mitarbeiternummer', 'SCHEMA', 'dbo', 'TABLE', 'Employees', 'COLUMN', 'EmployeeId'
execute sys.sp_addextendedproperty 'MS_Description', 'Vorname', 'SCHEMA', 'dbo', 'TABLE', 'Employees', 'COLUMN', 'FirstName'
execute sys.sp_addextendedproperty 'MS_Description', 'Nachname', 'SCHEMA', 'dbo', 'TABLE', 'Employees', 'COLUMN', 'LastName'
execute sys.sp_addextendedproperty 'MS_Description', 'Postleitzahl', 'SCHEMA', 'dbo', 'TABLE', 'Employees', 'COLUMN', 'Postcode'
execute sys.sp_addextendedproperty 'MS_Description', 'Ort', 'SCHEMA', 'dbo', 'TABLE', 'Employees', 'COLUMN', 'City'
execute sys.sp_addextendedproperty 'MS_Description', 'Straße und Hausnummer', 'SCHEMA', 'dbo', 'TABLE', 'Employees', 'COLUMN', 'StreetHouseNo'
execute sys.sp_addextendedproperty 'MS_Description', 'Einstellungsdatum', 'SCHEMA', 'dbo', 'TABLE', 'Employees', 'COLUMN', 'HireDate'
execute sys.sp_addextendedproperty 'MS_Description', 'Gehalt', 'SCHEMA', 'dbo', 'TABLE', 'Employees', 'COLUMN', 'Salary'
execute sys.sp_addextendedproperty 'MS_Description', 'Vorgesetzter', 'SCHEMA', 'dbo', 'TABLE', 'Employees', 'COLUMN', 'Manager'
execute sys.sp_addextendedproperty 'MS_Description', 'Mitarbeitertyp (M => Manager, V => Verkäufer, H => Handwerker, B => Büroangestellter, T => Trainer)', 'SCHEMA', 'dbo', 'TABLE', 'Employees', 'COLUMN', 'EmployeeType'
go

if object_id('Customers', 'U') is not null drop table [Customers]
go

create table [Customers](
  CustomerId    int         not null identity(1, 1),
  CustomerName  varchar(80) not null,
  Contact       varchar(80) not null,
  Postcode      char(5)     not null,
  City          varchar(50) not null,
  StreetHouseNo varchar(80) not null)
go

execute sys.sp_addextendedproperty 'MS_Description', 'Kunden' , 'SCHEMA', 'dbo', 'TABLE', 'Customers'
go

execute sys.sp_addextendedproperty 'MS_Description', 'Kundennummer', 'SCHEMA', 'dbo', 'TABLE', 'Customers', 'COLUMN', 'CustomerId'
execute sys.sp_addextendedproperty 'MS_Description', 'Name der Organisation', 'SCHEMA', 'dbo', 'TABLE', 'Customers', 'COLUMN', 'CustomerName'
execute sys.sp_addextendedproperty 'MS_Description', 'Ansprechpartner', 'SCHEMA', 'dbo', 'TABLE', 'Customers', 'COLUMN', 'Contact'
execute sys.sp_addextendedproperty 'MS_Description', 'Postleitzahl', 'SCHEMA', 'dbo', 'TABLE', 'Customers', 'COLUMN', 'Postcode'
execute sys.sp_addextendedproperty 'MS_Description', 'Ort', 'SCHEMA', 'dbo', 'TABLE', 'Customers', 'COLUMN', 'City'
execute sys.sp_addextendedproperty 'MS_Description', 'Straße und Hausnummer', 'SCHEMA', 'dbo', 'TABLE', 'Customers', 'COLUMN', 'StreetHouseNo'
go

if object_id('Products', 'U') is not null drop table [Products]
go

create table [Products](
  ProductId       int           not null identity(1, 1),
  ProductName     varchar(80)   not null,
  Material        char(2)       not null,
  Stock           int           not null,
  ProductCategory char(1)       not null,
  UnitPrice       decimal(8, 2) not null,
  WeaponType      char(2)       null,
  WeaponSize      char(1)       null,
  ArmorPart       char(2)       null)
go

execute sys.sp_addextendedproperty 'MS_Description', 'Produkte' , 'SCHEMA', 'dbo', 'TABLE', 'Products'
go

execute sys.sp_addextendedproperty 'MS_Description', 'Produktnummer', 'SCHEMA', 'dbo', 'TABLE', 'Products', 'COLUMN', 'ProductId'
execute sys.sp_addextendedproperty 'MS_Description', 'Produktname', 'SCHEMA', 'dbo', 'TABLE', 'Products', 'COLUMN', 'ProductName'
execute sys.sp_addextendedproperty 'MS_Description', 'Material (Ho => Holz, Me => Metall, St => Stoff, Le => Leder, Ke => Ketten, Pl => Platten)', 'SCHEMA', 'dbo', 'TABLE', 'Products', 'COLUMN', 'Material'
execute sys.sp_addextendedproperty 'MS_Description', 'Bestand', 'SCHEMA', 'dbo', 'TABLE', 'Products', 'COLUMN', 'Stock'
execute sys.sp_addextendedproperty 'MS_Description', 'Produktkategorie (W => Waffen, R => Rüstungen, P => Pferdezubehör)', 'SCHEMA', 'dbo', 'TABLE', 'Products', 'COLUMN', 'ProductCategory'
execute sys.sp_addextendedproperty 'MS_Description', 'Stückpreis', 'SCHEMA', 'dbo', 'TABLE', 'Products', 'COLUMN', 'UnitPrice'
execute sys.sp_addextendedproperty 'MS_Description', 'Waffentyp (Do => Dolch, Ax => Axt, Sc => Schwert, La => Langschwert, Bo => Bogen, Ar => Armbrust)', 'SCHEMA', 'dbo', 'TABLE', 'Products', 'COLUMN', 'WeaponType'
execute sys.sp_addextendedproperty 'MS_Description', 'Waffengröße', 'SCHEMA', 'dbo', 'TABLE', 'Products', 'COLUMN', 'WeaponSize'
execute sys.sp_addextendedproperty 'MS_Description', 'Rüstungsteil/Set (He => Helm, Br => Brustrüstung, Ha => Handschuh, Be => Beinrüstung, Sc => Schuh, Se => Set)', 'SCHEMA', 'dbo', 'TABLE', 'Products', 'COLUMN', 'ArmorPart'
go

if object_id('Courses', 'U') is not null drop table [Courses]
go

create table [Courses](
  CourseId        int           not null identity(1, 1),
  CourseName      varchar(80)   not null,
  PricePerTrainer decimal(6, 2) not null,
  Duration        int           not null)
go

execute sys.sp_addextendedproperty 'MS_Description', 'Gruppenkurse' , 'SCHEMA', 'dbo', 'TABLE', 'Courses'
go

execute sys.sp_addextendedproperty 'MS_Description', 'Kursnummer', 'SCHEMA', 'dbo', 'TABLE', 'Courses', 'COLUMN', 'CourseId'
execute sys.sp_addextendedproperty 'MS_Description', 'Kursname', 'SCHEMA', 'dbo', 'TABLE', 'Courses', 'COLUMN', 'CourseName'
execute sys.sp_addextendedproperty 'MS_Description', 'Preis je Trainer', 'SCHEMA', 'dbo', 'TABLE', 'Courses', 'COLUMN', 'PricePerTrainer'
execute sys.sp_addextendedproperty 'MS_Description', 'Dauer', 'SCHEMA', 'dbo', 'TABLE', 'Courses', 'COLUMN', 'Duration'
go

if object_id('Orders', 'U') is not null drop table [Orders]
go

create table [Orders](
  OrderId    int           not null identity(1, 1),
  OrderDate  date          not null default getdate(),
  OrderState char(1)       not null default 'O',
  CustomerId int           not null,
  Price      decimal(8, 2) not null default 0)
go

execute sys.sp_addextendedproperty 'MS_Description', 'Bestellungen' , 'SCHEMA', 'dbo', 'TABLE', 'Orders'
go

execute sys.sp_addextendedproperty 'MS_Description', 'Bestellnummer', 'SCHEMA', 'dbo', 'TABLE', 'Orders', 'COLUMN', 'OrderId'
execute sys.sp_addextendedproperty 'MS_Description', 'Bestelldatum', 'SCHEMA', 'dbo', 'TABLE', 'Orders', 'COLUMN', 'OrderDate'
execute sys.sp_addextendedproperty 'MS_Description', 'Bestellstatus (''O'' => Offen, ''A'' => Abgeschlossen, ''U'' => Ungültig)', 'SCHEMA', 'dbo', 'TABLE', 'Orders', 'COLUMN', 'OrderState'
execute sys.sp_addextendedproperty 'MS_Description', 'Kundennummer', 'SCHEMA', 'dbo', 'TABLE', 'Orders', 'COLUMN', 'CustomerId'
execute sys.sp_addextendedproperty 'MS_Description', 'Gesamtpreis', 'SCHEMA', 'dbo', 'TABLE', 'Orders', 'COLUMN', 'Price'
go

if object_id('OrderItems', 'U') is not null drop table [OrderItems]
go

create table [OrderItems](
  OrderId          int           not null,
  OrderLine        int           not null,
  ProductId        int           null,
  Quantity         int           null,
  UnitPrice        decimal(8, 2) null,
  IsRepair         tinyint       null,
  CourseId         int           null,
  ParticipantCount int           null)
go

execute sys.sp_addextendedproperty 'MS_Description', 'Bestellpositionen' , 'SCHEMA', 'dbo', 'TABLE', 'OrderItems'
go

execute sys.sp_addextendedproperty 'MS_Description', 'Bestellnummer', 'SCHEMA', 'dbo', 'TABLE', 'OrderItems', 'COLUMN', 'OrderId'
execute sys.sp_addextendedproperty 'MS_Description', 'Bestellpositionsnummer', 'SCHEMA', 'dbo', 'TABLE', 'OrderItems', 'COLUMN', 'OrderLine'
execute sys.sp_addextendedproperty 'MS_Description', 'Produktnummer', 'SCHEMA', 'dbo', 'TABLE', 'OrderItems', 'COLUMN', 'ProductId'
execute sys.sp_addextendedproperty 'MS_Description', 'Bestellmenge', 'SCHEMA', 'dbo', 'TABLE', 'OrderItems', 'COLUMN', 'Quantity'
execute sys.sp_addextendedproperty 'MS_Description', 'Stückpreis', 'SCHEMA', 'dbo', 'TABLE', 'OrderItems', 'COLUMN', 'UnitPrice'
execute sys.sp_addextendedproperty 'MS_Description', 'Reparatur? (Ansonsten Kauf)', 'SCHEMA', 'dbo', 'TABLE', 'OrderItems', 'COLUMN', 'IsRepair'
execute sys.sp_addextendedproperty 'MS_Description', 'Gruppenkursnummer', 'SCHEMA', 'dbo', 'TABLE', 'OrderItems', 'COLUMN', 'CourseId'
execute sys.sp_addextendedproperty 'MS_Description', 'Teilnehmeranzahl', 'SCHEMA', 'dbo', 'TABLE', 'OrderItems', 'COLUMN', 'ParticipantCount'
go

alter table [Employees]
add
  constraint PK_Employees primary key (EmployeeId),
  constraint FK_Employees_Employees foreign key (Manager) references [Employees] (EmployeeId),
  constraint CK_EmployeeType check (EmployeeType in ('M', 'V', 'H', 'B', 'T'))
go

alter table [Customers]
add
  constraint PK_Customers primary key (CustomerId)
go

alter table [Products]
add
  constraint PK_Products primary key (ProductId),
  constraint CK_Material check ((ProductCategory <> 'W' and Material in ('St', 'Le', 'Ke', 'Pl')) or (ProductCategory = 'W' and Material in ('Ho', 'Me'))),
  constraint CK_ProductCategory check (ProductCategory in ('W', 'R', 'P')),
  constraint CK_WeaponType check (ProductCategory <> 'W' or (ProductCategory = 'W' and WeaponType in ('Do', 'Ax', 'Sc', 'La', 'Bo', 'Ar'))),
  constraint CK_WeaponSize check (ProductCategory <> 'W' or (ProductCategory = 'W' and WeaponSize in ('S', 'M', 'L'))),
  constraint CK_ArmorPart check (ProductCategory <> 'R' or (ProductCategory = 'R' and ArmorPart in ('He', 'Br', 'Ha', 'Be', 'Sc', 'Se')))
go

alter table [Courses]
add
  constraint PK_Courses primary key (CourseId)
go

alter table [Orders]
add
  constraint PK_Orders primary key (OrderId),
  constraint FK_Orders_Customers foreign key (CustomerId) references [Customers] (CustomerId),
  constraint CK_OrderState check (OrderState in ('O', 'A', 'U'))
go

alter table [OrderItems]
add
  constraint PK_OrderItems primary key (OrderId, OrderLine),
  constraint FK_OrderItems_Orders foreign key (OrderId) references [Orders] (OrderId),
  constraint FK_OrderItems_Products foreign key (ProductId) references [Products] (ProductId),
  constraint FK_OrderItems_Courses foreign key (CourseId) references [Courses] (CourseId)
go

/*******************************************************************************
* 3.  Erstellung der Prozduren, Funktionen und Trigger
*******************************************************************************/
use [Myrtana]
go

if object_id('P_CreateEmployee', 'P') is not null drop procedure [P_CreateEmployee]
go

create procedure P_CreateEmployee(
  @FirstName      varchar(50),
  @LastName       varchar(50),
  @PostCode       varchar(5),
  @City           varchar(50),
  @StreetHouseNo  varchar(80),
  @Salary         decimal(6,2),
  @Manager        int,
  @EmployeeType   char(1))
as
begin

  begin try
    if exists (select * from Employees where @FirstName = FirstName and @LastName = LastName)
      throw 55000, 'Ein Mitarbeiter mit diesem Namen existiert bereits.', 1
    if @EmployeeType not in ('V', 'H', 'B', 'T', 'M')
      throw 55003, 'Der angegebene Mitarbeitertyp ist unbekannt.', 1

    if @Manager is not null begin
      if not exists (select * from Employees where EmployeeId = @Manager and Manager is null)
        throw 55001, 'Der Manager mit dieser Nummer existiert nicht.', 1
    end
    if exists (select * from Employees where @Manager = Manager and @EmployeeType = 'M') throw 55002, '', 1

    insert into Employees (FirstName, LastName, Postcode, City, StreetHouseNo, Salary, Manager, EmployeeType)
    values (@FirstName, @LastName, @Postcode, @City, @StreetHouseNo, @Salary, @Manager, @EmployeeType)

    print concat('Der Mitarbeiter mit dem Namen ', @LastName, ', ', @FirstName, ' wurde erfolgreich in der Datenbank angelegt.')
  end try

  begin catch
    if error_number() = 55000 begin
      print concat('Der Mitarbeiter mit dem Namen ', @LastName, ', s', @FirstName,  ' existiert bereits!')
    end
    else if error_number() = 55001 begin
      print concat('Der Manager mit der Nummer ', @Manager, ' existiert nicht!')
    end
    else if error_number() = 55002 begin
      print 'Manager können keine Manager haben.'
    end
    else if error_number() = 55003 begin
      print concat('Der Mitarbeitertyp ''', @EmployeeType, '''ist unbekannt!')
    end
    else begin
      print error_message()
    end
  end catch

end
go

use [Myrtana]
go

if object_id('P_UpdateEmployee', 'P') is not null drop procedure [P_UpdateEmployee]
go

create procedure P_UpdateEmployee(
  @EmployeeId     int,
  @FirstName      varchar(50)   = null,
  @LastName       varchar(50)   = null,
  @Postcode       varchar(5)    = null,
  @City           varchar(50)   = null,
  @StreetHouseNo  varchar(80)   = null,
  @Salary         decimal(6, 2) = null,
  @Manager        int           = null,
  @EmployeeType   char(1)       = null)
as
begin
  begin try

    if not exists (select * from Employees where @EmployeeId = EmployeeId)
      throw 55005, 'Der Mitarbeiter existiert nicht.', 1
    if @EmployeeType not in ('V', 'H', 'B', 'T', 'M')
      throw 55006, 'Dieser Mitarbeitertyp ist nich gestattet.', 1
    if @EmployeeType = 'M' begin
      set @Manager = null
    end
    if @Manager is not null begin
      if not exists (select * from Employees where @Manager = Manager)
        throw 55007, 'Der gewählte Manager existiert nicht.', 1
    end
    if @Manager is null and @EmployeeType = 'M' begin
      update  Employees
      set     Manager = @Manager
      where   EmployeeId = @EmployeeId
    end
    else begin
      update  Employees
      set     Manager = coalesce(@Manager, Manager)
      where   EmployeeId = @EmployeeId
    end

    update  Employees
    set     FirstName     = coalesce(@FirstName, FirstName),
            LastName      = coalesce(@LastName, LastName),
            Postcode      = coalesce(@Postcode, PostCode),
            City          = coalesce(@City, City),
            StreetHouseNo = coalesce(@StreetHouseNo, StreetHouseNo),
            Salary        = coalesce(@Salary, Salary),
            EmployeeType  = coalesce(@EmployeeType, EmployeeType)
    where   EmployeeId    = @EmployeeId

    print concat('Die Daten für den Mitarbeiter ', @LastName, ', ' , @FirstName, ' wurden erfolgreich editiert.')
  end try

  begin catch
    if error_number() = 55005 begin
      print 'Der Mitarbeiter existiert nicht.'
    end
    else if error_number() = 55006 begin
      print 'Dieser Mitarbeitertyp ist nich gestattet.'
    end
    else if error_number() = 55007 begin
      print 'Der gewählte Manager existiert nicht.'
    end
    else begin
      print error_message()
    end
  end catch

end
go

use [Myrtana]
go

if object_id('P_DeleteEmployee', 'P') is not null drop procedure [P_DeleteEmployee]
go

create procedure P_DeleteEmployee(@EmployeeId int)
as
begin

  begin try
    if not exists (select * from Employees where @EmployeeId = EmployeeId)
      throw 55004, 'Ein Mitarbeiter mit dieser Nummer existiert nicht.', 1
     
     delete from Employees
     where EmployeeId = @EmployeeId

     print 'Der Mitarbeiter wurde erfolgreich gelöscht.'
  end try


  begin catch
    if error_number() = 55004 begin
      print concat('Der Mitarbeiter ', @EmployeeId, ' existiert nicht.')
    end
    else begin
      print error_message()
    end
  end catch

end
go

use [Myrtana]
go

if object_id('P_CreateProduct', 'P') is not null drop procedure [P_CreateProduct]
go

create procedure P_CreateProduct(
  @ProductName      varchar(80),
  @Material         char(2),
  @UnitPrice        decimal(8, 2),
  @Stock            int,
  @ProductCategory  char(1),
  @WeaponType       char(2) = null,
  @WeaponSize       char(1) = null,
  @ArmorPart        char(2) = null)
as
begin

  begin try
    if exists (select * from Products where @ProductName = ProductName) throw 65000, 'Produkt vorhanden', 1
    if @ProductCategory not in ('W', 'R', 'P') throw 65003, 'Keine existiertende Kategorie', 1

    if @ProductCategory = 'W' begin
      if @ArmorPart is not null throw 65001, 'Waffe != Rüstung', 1
      if @WeaponType not in ('Do', 'Ax', 'Sc', 'La', 'Bo', 'Ar') throw 65008, 'Waffentyp existiert nicht', 1
      if @WeaponSize not in ('S', 'M', 'L') throw 65009, 'Waffengröße existiert nicht', 1
      if @Material not in ('Ho', 'Me') throw 65010, 'Waffenmatrial existiert nicht', 1

      insert into Products (ProductName, Material, Stock, ProductCategory, UnitPrice, WeaponType, WeaponSize)
      values (@ProductName, @Material, @Stock, @ProductCategory, @UnitPrice, @WeaponType, @WeaponSize)
    end
    else if @ProductCategory = 'R' begin
      if @WeaponSize is not null or @WeaponType is not null throw 65002, 'Rüstung != Waffe', 1
      if @Material not in ('St', 'Le', 'Ke', 'Pl') throw 65011, 'Rüstungsmaterial existiert nicht', 1
      if @ArmorPart not in ('He', 'Br', 'Ha', 'Be', 'Sc', 'Se') throw 65012, 'Rüstungsteil existiert nicht',1
      if @Material = 'Ke' and @ArmorPart = 'Se' throw 65013, 'Kettenset existiert nicht', 1

      insert into Products (ProductName, Material, Stock, ProductCategory, UnitPrice, ArmorPart)
      values (@ProductName, @Material, @Stock, @ProductCategory, @UnitPrice, @ArmorPart)
    end
    else begin
      insert into Products (ProductName, Material, Stock, ProductCategory)
      values (@ProductName, @Material, @Stock, @ProductCategory)
    end

    print concat('Das Produkt mit dem Namen ', @ProductName, ' wurde erfolgreich erstellt.')
  end try

  begin catch
    if error_number() = 65000 begin
      print concat('Das Produkt ', @ProductName, ' existiert bereits.')
    end
    else if error_number() in (65001, 65002) begin
      print concat('Das Produkt mit dem Namen ', @ProductName, ' wurde als falscher Typ deklariert.')
    end
    else if error_number() = 65003 begin
      print 'Die eingegebene Kategorie existiert nicht.'
    end
    else if error_number() = 65008 begin
      print 'Der Waffentyp existiert nicht'
    end
    else if error_number() = 65009 begin
      print 'Die Waffengröße existiert nicht'
    end
    else if error_number() in (65010, 65011) begin
      print 'Das Material wird nicht angeboten'
    end
    else if error_number() = 65012 begin
      print 'Das Setteil existiert nicht'
    end
    else if error_number() = 65013 begin
      print 'Ein Kettenset existiert nicht'
    end
    else begin
      print error_message()
    end
  end catch
end
go

use [Myrtana]
go

if object_id('P_UpdateProduct', 'P') is not null drop procedure [P_UpdateProduct]
go

create procedure P_UpdateProduct(
  @ProductId        int,
  @ProductName      varchar(80) = null,
  @Material         char(2)     = null,
  @Stock            int         = null,
  @ProductCategory  char(1)     = null,
  @WeaponType       char(2)     = null,
  @WeaponSize       char(1)     = null,
  @ArmorPart        char(2)     = null)
as
begin

  begin try
    declare @cat_old char(1)

    select @cat_old = ProductCategory from Products

    if not exists (select * from Products where @ProductId = ProductId) throw 65004, '', 1
    if @cat_old != @ProductCategory and @ProductCategory is not null throw 65007, '', 1

    if @cat_old = 'W' begin
      update Products
      set   ProductName = coalesce(@ProductName, ProductName),
            Material    = coalesce(@Material, Material),
            Stock       = coalesce(@Stock, Stock),
            WeaponType  = coalesce(@WeaponType, WeaponType),
            WeaponSize  = coalesce(@WeaponSize, WeaponSize)
      where ProductId   = @ProductId
    end
    else if @cat_old = 'R' begin
      update  Products
        set   ProductName = coalesce(@ProductName, ProductName),
              Material    = coalesce(@Material, Material),
              Stock       = coalesce(@Stock, Stock),
              ArmorPart   = coalesce(@ArmorPart, ArmorPart)
        where ProductId   = @ProductId
    end
    else begin
      update Products
      set   ProductName = coalesce(@ProductName, ProductName),
            Material = coalesce(@Material, Material),
          Stock = coalesce(@Stock, Stock)
      where ProductId = @ProductId
    end

    select  @ProductName = ProductName
    from    Products
    where   @ProductId = ProductId

    print concat ('Das Produkt mit der Nummer ', @ProductId, ' wurde erfolgreich geändert.')
  end try

  begin catch
    if error_number() = 65004 begin
      print concat('Ein Produkt mit der Nummer ', @ProductId, ' existiert nicht.')
    end
    else if error_number() = 65007 begin
      print 'Die Produkkategorie kann nicht geändert werden.'
    end
    else begin
      print error_message()
    end
  end catch

end
go

use [Myrtana]
go

if object_id('P_DeleteProduct', 'P') is not null drop procedure [P_DeleteProduct]
go

create procedure P_DeleteProduct(@ProductId int)
as
begin

  begin try
    declare @Stock int

    select  @Stock = Stock
    from    Products
    where   @ProductId = ProductId

    if not exists (select * from Products where @ProductId = ProductId) throw 65005, '', 1

    if @Stock > 0 throw 65006, '', 1

    delete from Products where ProductId = @ProductId

    print concat('Das Produkt mit der Nummer ', @ProductId, ' wurde erfolgreich gelöscht.')
  end try

  begin catch
    if error_number() = 65005 begin
      print concat('Ein Produkt mit der Nummer ', @ProductId, ' existiert nicht.')
    end
    else if error_number() = 65006 begin
      print concat('Das Produkt mit der Nummer ', @ProductId, ' kann nicht gelöscht werden, da noch ', @Stock, ' Stück auf Lager sind.')
    end
    else begin
      print error_message()
    end
  end catch

end
go

if object_id('FN_CalcProductionPrice', 'FN') is not null drop function [FN_CalcProductionPrice]
go

create function FN_CalcProductionPrice(@ProductId int) returns decimal(8, 2)
as
begin
  declare @UnitPrice decimal(8,2)
  declare @ProductionPrice decimal(8,2)

  select  @UnitPrice = UnitPrice
  from    Products
  where   ProductId = @ProductId

  set @ProductionPrice = (@UnitPrice / 100.0) * 63.0

  return @ProductionPrice
end
go

if object_id('FN_CalcRepair', 'FN') is not null drop function [FN_CalcRepair]
go

create function FN_CalcRepair(@ProductId int, @RepairPercent tinyint)
returns decimal(8, 2)
as
begin
  declare @RepairCost decimal(8, 2)
  declare @actualPrice decimal(8, 2)
  declare @RepairContent decimal(5, 3)

  if @RepairPercent >= 100 begin
    set @RepairCost = -1.0
  end
  else begin
    set @RepairContent = 63.0 / 100.0 * @RepairPercent

    select  @actualPrice = UnitPrice
    from    Products
    where   ProductId = @ProductId

    set @RepairCost = (@actualPrice / 100.0) * @RepairContent
  end

  return @RepairCost
end
go

use [Myrtana]
go

if object_id('P_CreateCourse', 'P') is not null drop procedure P_CreateCourse
go

create procedure P_CreateCourse(
  @CourseName       varchar(80),
  @PricePerTrainer  decimal(6, 2),
  @Duration         int)
as
begin

  begin try
    if exists (select * from Courses where CourseName = @CourseName)
      throw 80000, 'Dieser Kursname ist bereits vergeben.', 1
    if @PricePerTrainer < 0
      throw 80001,'Die Kosten pro Trainer und Kurs liegen unter 0.', 1
    if @Duration < 0
      throw 80002, 'Die Dauer ist negativ.', 1

    insert into Courses (CourseName, PricePerTrainer, Duration)
    values (@CourseName, @PricePerTrainer, @Duration)

    print concat('Der Kurs mit dem Namen ''', @CourseName, ''' wurde erfolgreich angelegt.')
  end try

  begin catch
    if error_number() = 80000 begin
      print concat('Es gibt bereits einen Kurs mit dem Namen ''', @CourseName, '''!')
    end
    else if error_number() = 80001 begin
      print concat('Der Preis pro Trainer liegt bei ', @PricePerTrainer, ' . Es muss ein positiver Wert eingetragen werden!')
    end
    else if error_number() = 80002 begin
      print concat('Die Dauer liegt bei ', @Duration,' . Es muss ein positiver Wert eingetragen werden!')
    end
    else begin
      print error_message()
    end
  end catch

end
go

use [Myrtana]
go

if object_id('P_UpdateCourse', 'P') is not null drop procedure P_UpdateCourse
go

create procedure P_UpdateCourse(
  @CourseId         int,
  @CourseName       varchar(80),
  @PricePerTrainer  decimal(6, 2),
  @Duration         int)
as
begin

  begin try
    if not exists (select * from Courses where CourseId = @CourseId)
      throw 80003, 'Diese Kursnummer existiert nicht.', 1
    if @PricePerTrainer < 0
      throw 80001, 'Die Kosten pro Trainer und Kurs liegen unter 0.', 1
    if @Duration < 0
      throw 80002, 'Die Dauer ist negativ.', 1

    update  Courses
    set     CourseName      = coalesce(@CourseName, CourseName),
            PricePerTrainer = coalesce(@PricePerTrainer, PricePerTrainer),
            Duration        = coalesce(@Duration, Duration)
    where   CourseId        = @CourseId

    print concat('Der Kurs mit der Nummer ', @CourseId, ' wurde erfolgreich geändert.')
  end try

  begin catch
    if error_number() = 80001 begin
      print 'Der Preis je Trainer darf nicht negativ sein!'
    end
    else if error_number() = 80002 begin
      print 'Die Dauer des Kurses darf nicht negativ sein!'
    end
    else if error_number() = 80003 begin
      print concat('Ein Kurs mit der Nummer ', @CourseId, ' existiert nicht!')
    end
    else begin
      print error_message()
    end
  end catch

end
go

use [Myrtana]
go

if object_id('P_DeleteCourse', 'P') is not null drop procedure P_DeleteCourse
go

create procedure P_DeleteCourse(@CourseId int)
as
begin

  begin try
    if not exists (select * from Courses where CourseId = @CourseId)
      throw 80003, 'Diese Kursnummer existiert nicht.', 1

    delete from courses where CourseId = @CourseId

    print concat('Der Kurs mit der Nummer ', @CourseId, ' wurde erfolgreich gelöscht.')
  end try

  begin catch
    if error_number() = 80003 begin
      print concat('Ein Kurs mit der Nummer ', @CourseId, ' existiert nicht!')
    end
    else begin
      print error_message()
    end
  end catch

end
go

use [Myrtana]
go

if object_id('P_CreateOrderItem', 'P') is not null drop procedure [P_CreateOrderItem]
go
create procedure P_CreateOrderItem(
  @OrderId int,
  @ProductId int = null,
  @Quantity int = null,
  @UnitPrice decimal(8, 2) = null,
  @IsRepair tinyint = null,
  @CourseId int = null,
  @ParticipantCount int = null)
as
begin

  begin try
    if not exists (select * from Orders where OrderId = @OrderId)
      throw 51201, 'Eine Bestellung mit dieser Nummer existiert nicht.', 1

    declare @OrderState char(1)

    select  @OrderState = OrderState
    from    Orders
    where   OrderId = @OrderId

    if @OrderState = 'A'
      throw 51202, 'Die Bestellung ist bereits abgeschlossen.', 1

    if @OrderState = 'U'
      throw 51203, 'Die Bestellung wurde verworfen.', 1

    if @ProductId is not null begin
      if not exists (select * from Products where ProductId = @ProductId)
        throw 51204, 'Ein Artikel mit dieser Artikelnummer existiert nicht.', 1

      -- Bestand abrechnen, den Rest zurückgeben
      declare @Stock int

      update  Products
      set     Stock = Stock - @Quantity
      where   ProductId = @ProductId

      select  @Stock = Stock - @Quantity
      from    Products
      where   ProductId = @ProductId

      -- Wenn nicht genug Bestand, dann Abbruch.
      if @Stock < 0 begin
        update  Products
        set     Stock = Stock + @Quantity
        where   ProductId = @ProductId

        if 1 = 1 throw 51205, 'Vom Artikel mit dieser Nummer ist nicht genug Bestand verfügbar.', 1
      end

      select @UnitPrice = UnitPrice from Products where ProductId = @ProductId

      insert into OrderItems (OrderId, ProductId, Quantity, UnitPrice, IsRepair)
      values (@OrderId, @ProductId, @Quantity, @UnitPrice, @IsRepair)

      update  Orders
      set     Price = Price + (@UnitPrice * @Quantity)
      where   OrderId = @OrderId

      if @IsRepair = 1 begin
        print 'Der Bestellung wurde erfolgreich eine Warenposition hinzugefügt.'
      end
      else begin
        print 'Der Bestellung wurde erfolgreich eine Reparatur hinzugefügt.'
      end
    end
    else if @CourseId is not null begin
      if not exists (select * from Courses where CourseId = @CourseId)
        throw 51206, 'Ein Kurs mit dieser Nummer existiert nicht.', 1

      declare @Price decimal(8, 2)

      select  @Price = PricePerTrainer * ceiling(@ParticipantCount / 12.0)
      from    Courses
      where   CourseId = @CourseId

      insert into OrderItems (OrderId, CourseId, ParticipantCount)
      values (@OrderId, @CourseId, @ParticipantCount)

      update  Orders
      set     Price = Price + @Price
      where   OrderId = @OrderId

      print 'Der Bestellung wurde erfolgreich eine Gruppenkursbuchung hinzugefügt.'
    end
    else
      throw 51207, 'Für die Erstellung einer Bestellposition muss entweder eine Produktnummer oder eine Kursnummer übergeben werden', 1
  end try

  begin catch
    if error_number() = 51201 begin
      print concat('Eine Bestellung mit der Nummer ', @OrderId, ' existiert nicht!')
    end
    else if error_number() = 51202 begin
      print concat('Die Bestellung mit der Nummer ', @OrderId, ' ist bereits abgeschlossen!')
    end
    else if error_number() = 51203 begin
      print concat('Die Bestellung mit der Nummer ', @OrderId, ' wurde verworfen.!')
    end
    else if error_number() = 51205 begin
      print concat('Vom Artikel mit der Nummer ', @ProductId, ' sind ', -@Stock, ' Stück zu wenig verfügbar!')
    end
    else if error_number() = 51206 begin
      print concat('Ein Kurs mit der Nummer ', @CourseId, ' existiert nicht!')
    end
    else if error_number() = 51207 begin
      print 'Für die Erstellung einer Bestellposition muss entweder eine Produktnummer oder eine Kursnummer übergeben werden!'
    end
    else begin
      print error_message()
    end
  end catch

end
go

use [Myrtana]
go

if object_id('TR_OrderItems', 'TR') is not null drop trigger TR_OrderItems;
go

create trigger TR_OrderItems
on OrderItems
instead of insert, update
as
begin
  declare @Inserting  bit = 0
  declare @Deleting   bit = 0

  if exists (select * from Inserted) set @Inserting = 1
  if exists (select * from Deleted) set @Deleting = 1

  declare @OrderId int

  select @OrderId = OrderId
  from Inserted

  if @Deleting = 0 begin
    declare @OrderLine  int

    select @OrderLine = coalesce(max(OrderLine), 0) + 1
    from OrderItems
    where OrderId = @OrderId

    insert into OrderItems (OrderId, OrderLine, ProductId, Quantity, UnitPrice, IsRepair, CourseId, ParticipantCount)
      select  OrderId, @OrderLine, ProductId, Quantity, UnitPrice, IsRepair, CourseId, ParticipantCount
      from    Inserted
  end
  else begin
    update  OrderItems
    set     ProductId = (select ProductId from Inserted),
            Quantity = (select Quantity from Inserted),
            UnitPrice = (select UnitPrice from Inserted),
            IsRepair = (select IsRepair from Inserted),
            CourseId = (select CourseId from Inserted),
            ParticipantCount = (select ParticipantCount from Inserted)
    where   OrderId = @OrderId
  end
end
go

use [Myrtana]
go

if object_id('P_NewOrder', 'P') is not null drop procedure [P_NewOrder]
go
create procedure P_NewOrder(
  @CustomerId int)
as
begin

  begin try
    if not exists (select * from Customers where CustomerId = @CustomerId)
      throw 51101, 'Ein Kunde mit diser Kundennummer existiert nicht', 1

    insert into Orders (CustomerId)
    values (@CustomerId)

    print 'Ein Bestellkopf wurde erfolgreich angelegt.'
  end try

  begin catch
    if error_number() = 51101 begin
      print concat('Ein Kunde mit der Kundennummer ', @CustomerId, ' existiert nicht!')
    end
    else begin
      print error_message()
    end
  end catch

end
go

use [Myrtana]
go

if object_id('P_SetOrderState', 'P') is not null drop procedure [P_SetOrderState]
go
create procedure P_SetOrderState(
  @OrderId int,
  @OrderState char(1))
as
begin
  begin try
    if not exists (select * from Orders where OrderId = @OrderId)
      throw 51301, 'Eine Bestellung mit dieser Nummer existiert nicht.', 1

    declare @OldOrderState char(1)

    select  @OldOrderState = OrderState
    from    Orders
    where   OrderId    = @OrderId

    if @OldOrderState = 'A'
      throw 51302, 'Diese Bestellung ist bereits abgeschlossen.', 1

    update  Orders
    set     OrderState = @OrderState
    where   OrderId    = @OrderId
  end try
  begin catch
    if error_number() = 51301 begin
      print concat('Eine Bestellung mit der Nummer ', @OrderId, ' existiert nicht!')
    end
    else if error_number() = 51302 begin
      print concat('Diese Bestellung mit der Nummer ', @OrderId, ' ist bereits abgeschlossen!')
    end
    else begin
      print error_message()
    end
  end catch
end
go

use [Myrtana]
go

if object_id('P_CreateCustomer', 'P') is not null drop procedure P_CreateCustomer
go

create procedure P_CreateCustomer(
  @CustomerName   varchar(80),
  @Contact        varchar(80),
  @Postcode       varchar(5),
  @City           varchar(50),
  @StreetHouseNo  varchar(80))
as
begin

  begin try
    if exists (select * from Customers where @CustomerName = CustomerName)
      throw 50001, 'Ein Kunde mit diesem Namen existiert bereits', 1

    insert into Customers (CustomerName, Contact, Postcode, City, StreetHouseNo)
    values (@CustomerName, @Contact, @Postcode, @City, @StreetHouseNo)

    print concat('Der Kunde ''', @CustomerName, ''' wurde erfolgreich angelegt.')
  end try

  begin catch
    if error_number() = 50001 begin
      print concat('Der Kunde mit dem Namen ', @CustomerName, ' existiert bereits.')
    end
    else begin
      print error_message()
    end
  end catch

end
go

use [Myrtana]
go

if object_id('P_UpdateCustomer', 'P') is not null drop procedure P_UpdateCustomer
go

create procedure P_UpdateCustomer(
  @CustomerId     int,
  @CustomerName   varchar(80) = null,
  @Contact        varchar(80) = null,
  @Postcode       varchar(5) = null,
  @City           varchar(50) = null,
  @StreetHouseNo  varchar(80) = null)
as
begin
  begin try
    if not exists (select * from Customers where @CustomerId = CustomerId)
      throw 50002, 'Ein Kunde mit dieser Nummer existiert nicht.', 1

    update  Customers
    set     CustomerName  = coalesce(@CustomerName, CustomerName),
            Contact       = coalesce(@Contact, Contact),
            Postcode      = coalesce(@Postcode, Postcode),
            City          = coalesce(@City, City),
            StreetHouseNo = coalesce(@StreetHouseNo, StreetHouseNo)
    where CustomerId = @CustomerId

    select  @CustomerName = CustomerName
    from    Customers
    where   @CustomerId = CustomerId
    print concat('Die Kundendaten von (', @CustomerName, ') wurden erfolgreich editiert.')
  end try

  begin catch
    if error_number() = 50002 begin
      print concat('Der Kunde mit der Nummer ', @CustomerId, ' existiert nicht.')
    end
    else begin
      print error_message()
    end
  end catch
end
go

use [Myrtana]
go

if object_id('P_DeleteCustomer', 'P') is not null drop procedure P_DeleteCustomer
go

create procedure P_DeleteCustomer(@CustomerId int)
as
begin

  begin try
    if not exists (select * from Customers where @CustomerId = CustomerId)
      throw 50001, 'Ein Kunde mit dieser Nummer existiert nicht.', 1

    delete from Customers
    where CustomerId = @CustomerId

    print concat('Der Kunde mit der Nummer ', @CustomerId, ' wurde erfolgreich gelöscht.')
  end try

  begin catch
    if error_number() = 50001 begin
      print concat ('Der Kunde ', @CustomerId, ' existiert nicht.')
    end
    else begin
      print error_message()
    end
  end catch

end
go

/*******************************************************************************
* 4.  Grundlegende Befüllung der Tabellen
*******************************************************************************/

--------------------------------------------------------------------------------
set identity_insert [Employees] on
go

insert into Employees (EmployeeId, FirstName, LastName, Postcode, City, StreetHouseNo, HireDate, Salary, Manager, EmployeeType)
values
  (1, 'Pain', 'Fabian', '49099', 'Erfurt', 'Malburger Platz 3', '2018-07-12', 215.99, null, 'M'),
  (2, 'Savas', 'Peter', '08351', 'Berlin', 'Goethestr. 2', '2018-07-12', 215.99, null, 'M')
go

set identity_insert [Employees] off
go

--------------------------------------------------------------------------------
set identity_insert [Customers] on
go

insert into Customers (CustomerId, CustomerName, Contact, Postcode, City, StreetHouseNo)
values
  (1, 'Hugh Anderson', 'Rubber Duck', '99086', 'Erfurt', 'Juri-Gagarin-Ring. 3'),
  (2, 'John Anderson', 'Phil Dunphy', '99089', 'Erfurt', 'Schapirostraße. 10'),
  (3, 'Andreas Malikum', 'John Anderson', '99086', 'Erfurt', 'Berliner-Straße. 9'),
  (4, 'Phil Dunphy', 'John Anderson', '85051', 'Berlin', 'Münchener-Straße. 9'),
  (5, 'Andrea Cambiolo', 'John Anderson', '99086', 'Erfurt', 'Leipziger-Platz. 9'),
  (6, 'Felix Huber', 'John Anderson', '99088', 'Erfurt', 'Grünwald-Str. 85'),
  (7, 'Peter Lustig', 'Laura Dark', '69053', 'Fürth', 'Berliner-Straße. 9'),
  (8, 'Laura Dark', 'Robin Vollmer', '99086', 'Erfurt', 'Daberstedt-Str. 2'),
  (9, 'Pascal Levain', 'John Anderson', '99086', 'Erfurt', 'Am DMV. 25'),
  (10, 'Andre Levain', 'John Anderson', '25598', 'Los Santos', 'Grovestreet. 1')
go

set identity_insert [Customers] off
go

--------------------------------------------------------------------------------
set identity_insert [Products] on
go

insert into Products (ProductId, ProductName, Material, Stock, ProductCategory, UnitPrice, WeaponType, WeaponSize, ArmorPart)
values
  (1, 'Kleiner Holzdolch', 'Ho', 1180, 'W', 30.00, 'Do', 'S', null),
  (2, 'Holzdolch', 'Ho', 1200, 'W', 35.00, 'Do', 'M', null),
  (3, 'Großer Holzdolch', 'Ho', 1200, 'W', 40.00, 'Do', 'L', null),
  (4, 'Kleiner Metalldolch', 'Me', 1200, 'W', 150.00, 'Do', 'S', null),
  (5, 'Metalldolch', 'Me', 1200, 'W', 160.00, 'Do', 'M', null),
  (6, 'Großer Metalldolch', 'Me', 1200, 'W', 170.00, 'Do', 'L', null),
  (7, 'Kleine Holzaxt', 'Ho', 1200, 'W', 40.00, 'Ax', 'S', null),
  (8, 'Holzaxt', 'Ho', 1200, 'W', 45.00, 'Ax', 'M', null),
  (9, 'Große Holzaxt', 'Ho', 1200, 'W', 50.00, 'Ax', 'L', null),
  (10, 'Kleine Metallaxt', 'Me', 1200, 'W', 190.00, 'Ax', 'S', null),
  (11, 'Metallaxt', 'Me', 5, 'W', 200.00, 'Ax', 'M', null),
  (12, 'Große Metallaxt', 'Me', 1200, 'W', 210.00, 'Ax', 'L', null),
  (13, 'Kleines Holzschwert', 'Ho', 0, 'W', 50.00, 'Sc', 'S', null),
  (14, 'Holzschwert', 'Ho', 1200, 'W', 55.00, 'Sc', 'M', null),
  (15, 'Großes Holzschwert', 'Ho', 1200, 'W', 60.00, 'Sc', 'L', null),
  (16, 'Kleines Metallschwert', 'Me', 1200, 'W', 200.00, 'Sc', 'S', null),
  (17, 'Metallschwert', 'Me', 1200, 'W', 225.00, 'Sc', 'M', null),
  (18, 'Großes Metallschwert', 'Me', 1200, 'W', 250.00, 'Sc', 'L', null),
  (19, 'Kleines Holzlangschwert', 'Ho', 1200, 'W', 60.00, 'Sc', 'S', null),
  (20, 'Holzlangschwert', 'Ho', 1200, 'W', 65.00, 'Sc', 'M', null),
  (21, 'Großes Holzlangschwert', 'Ho', 1200, 'W', 70.00, 'Sc', 'L', null),
  (22, 'Kleines Metalllangschwert', 'Me', 1200, 'W', 250.00, 'Sc', 'S', null),
  (23, 'Metalllangschwert', 'Me', 1200, 'W', 275.00, 'Sc', 'M', null),
  (24, 'Großes Metalllangschwert', 'Me', 1200, 'W', 300.00, 'Sc', 'L', null),
  (25, 'Kleiner Bogen', 'Ho', 1200, 'W', 70.00, 'Sc', 'S', null),
  (26, 'Bogen', 'Ho', 1200, 'W', 75.00, 'Sc', 'M', null),
  (27, 'Großer Bogen', 'Ho', 1200, 'W', 80.00, 'Sc', 'L', null),
  (28, 'Kleine Armbrust', 'Ho', 1200, 'W', 70.00, 'Sc', 'S', null),
  (29, 'Armbrust', 'Ho', 1200, 'W', 75.00, 'Sc', 'M', null),
  (30, 'Große Armbrust', 'Ho', 1200, 'W', 80.00, 'Sc', 'L', null),
  (31, 'Stoffhelm', 'St', 1000, 'R', 40.00, null, null, 'He'),
  (32, 'Stoffbrustrüstung', 'St', 1000, 'R', 150.00, null, null, 'Br'),
  (33, 'Stoffhandschuh', 'St', 1000, 'R', 50.00, null, null, 'Ha'),
  (34, 'Stoffbeinrüstung', 'St', 1000, 'R', 130.00, null, null, 'Be'),
  (35, 'Stoffschuh', 'St', 1000, 'R', 70.00, null, null, 'Sc'),
  (36, 'Lederhelm', 'Le', 1000, 'R', 40.00, null, null, 'He'),
  (37, 'Lederbrustrüstung', 'Le', 1000, 'R', 150.00, null, null, 'Br'),
  (38, 'Lederhandschuh', 'Le', 1000, 'R', 50.00, null, null, 'Ha'),
  (39, 'Lederbeinrüstung', 'Le', 1000, 'R', 130.00, null, null, 'Be'),
  (40, 'Lederschuh', 'Le', 1000, 'R', 70.00, null, null, 'Sc'),
  (41, 'Plattenhelm', 'Pl', 1000, 'R', 40.00, null, null, 'He'),
  (42, 'Plattenbrustrüstung', 'Pl', 1000, 'R', 150.00, null, null, 'Br'),
  (43, 'Plattenhandschuh', 'Pl', 1000, 'R', 50.00, null, null, 'Ha'),
  (44, 'Plattenbeinrüstung', 'Pl', 1000, 'R', 130.00, null, null, 'Be'),
  (45, 'Plattenschuh', 'Pl', 1000, 'R', 70.00, null, null, 'Sc'),
  (46, 'Stoffrüstungsset', 'St', 1000, 'R', 400.00, null, null, 'Se'),
  (47, 'Lederrüstungsset', 'Le', 1000, 'R', 800.00, null, null, 'Se'),
  (48, 'Plattenrüstungsset', 'Pl', 1000, 'R', 1200.00, null, null, 'Se'),
  (49, 'Schwert für Kinder', 'Me', 1200, 'W', 30.00, 'Sc', 'M', null),
  (50, 'Lederrüstung für Kinder', 'Le', 1000, 'R', 800.00, null, null, 'Se')
go

set identity_insert [Products] off
go

--------------------------------------------------------------------------------
set identity_insert [Courses] on
go

insert into Courses (CourseId, CourseName, PricePerTrainer, Duration)
values
  (1, 'Schwerttraining-Grünschnabel', 100.00, 1),
  (2, 'Schwerttraining-Knappe', 200.00, 3),
  (3, 'Schwerttraining-Experte', 250.00, 5),
  (4, 'Sprachtraining-Anfänger', 75.00, 1),
  (5, 'Sprachtraining-Fortgeschritten', 150.00, 3),
  (6, 'Sprachtraining-Experte', 200.00, 5)
go

set identity_insert [Courses] off
go

--------------------------------------------------------------------------------
set identity_insert [Orders] on
go

insert into Orders (OrderId, OrderDate, OrderState, CustomerId, Price)
values
  (1, '2018-07-12', 'A', 1, 1000.00)
go

set identity_insert [Orders] off
go

--------------------------------------------------------------------------------
insert into OrderItems (OrderId, ProductId, Quantity, UnitPrice, IsRepair, CourseId, ParticipantCount)
values (1, 1, 13, 30.00, 0, null, null)
go

insert into OrderItems (OrderId, ProductId, Quantity, UnitPrice, IsRepair, CourseId, ParticipantCount)
values (1, 1, 7, 30.00, 1, null, null)
go

insert into OrderItems (OrderId, ProductId, Quantity, UnitPrice, IsRepair, CourseId, ParticipantCount)
values (1, null, null, null, null, 2, 15)
go

--------------------------------------------------------------------------------

/*******************************************************************************
* 5.  Grundlegende Tests der Prozeduren, Funktionen und Trigger
*******************************************************************************/

use [Myrtana]
go

--------------------------------------------------------------------------------
-- Testszenarien zum Erstellen von Kunden

execute dbo.P_CreateCustomer 'Theatergruppe 1349', 'Hans Meier', 25598, 'Erfurt', 'Südstrasse 1'

--------------------------------------------------------------------------------
-- Testszenarien zum Erstellen von Mitarbeitern

execute dbo.P_CreateEmployee 'Max', 'Mustermann', '54545', 'Trier','Karl-Marx-Str. 1', 255.99, 2, 'B'

--------------------------------------------------------------------------------
-- Löschprozesse zu den Kunden/Mitarbeiterdatensätze

-- Löschung eines Kundens mit der entsprechenden ID
execute dbo.P_DeleteCustomer 2

-- Löschung eines Mitarbeiters mit der entsprechenden ID
execute dbo.P_DeleteEmployee 14

--------------------------------------------------------------------------------
-- Update-Prozesse der Kunden/Mitarbeiterdatensätze

-- Änderung des Wohnsitzes
execute dbo.P_UpdateCustomer 4, @City = 'Berlin'

-- Änderung des Namens, des Manager Tags sowie der Vergütung
execute dbo.P_UpdateEmployee 5, @LastName = 'Hansen', @EmployeeType = 'M', @Salary = 339.00

--------------------------------------------------------------------------------
-- Produkt-Erstellung

execute dbo.P_CreateProduct 'Schwert für Kinder', 'Me', 30.0, 1200, @ProductCategory = 'W', @WeaponType = 'Sc', @WeaponSize = 'M'
execute dbo.P_CreateProduct 'Lederrüstung für Kinder', 'Le', 800.0, 1000, 'R', @ArmorPart = 'Se'

--------------------------------------------------------------------------------
-- Produkt-Änderung

-- Änderungen des Lagerbestandes mit der ID 13
execute dbo.P_UpdateProduct 13, @Stock = 0

-- Änderungen des Lagerbestandes mit der ID 11
execute dbo.P_UpdateProduct 11, @Stock = 5

--------------------------------------------------------------------------------
-- Produkt-Löschung

-- Entfernung des Produktes mit der ID 13 aus dem Sortiment
execute dbo.P_DeleteProduct 13

--------------------------------------------------------------------------------

print '--------------------'
print '-- Function-Tests:'

print concat('FN_CalcProductionPrice(1): ', dbo.FN_CalcProductionPrice(1))

print concat('FN_CalcRepair(64):  ', dbo.FN_CalcRepair(1, 63))
print concat('FN_CalcRepair(100): ', dbo.FN_CalcRepair(1, 100))

--------------------------------------------------------------------------------
-- Kurs-Erstellung

-- Erstellung der Training Kurse
execute dbo.P_CreateCourse 'Training-Anfänger', 100, 1

--------------------------------------------------------------------------------
-- Kurs-Änderungen

-- Änderung des Kursnamens
execute dbo.P_UpdateCourse 1, 'Schwerttraining-Grünschnabel', 100, 1

-- Änderung des Kursnamens
execute dbo.P_UpdateCourse 2, 'Schwerttraining-Knappe', 200, 3

--------------------------------------------------------------------------------
-- Kurs-Löschung

-- Löscht den Kurs mit der ID 1
execute dbo.P_DeleteCourse 1

--------------------------------------------------------------------------------
-- Bestellungen anlegen
-- (neuer Bestellkopf, neue Bestellposition, Bestellung abschließen)
execute dbo.P_NewOrder @CustomerId = 1
go

execute dbo.P_CreateOrderItem 2, @ProductId = 1, @Quantity = 13, @IsRepair = 0
go
execute dbo.P_CreateOrderItem 2, @ProductId = 1, @Quantity = 7, @IsRepair = 1
go
execute dbo.P_CreateOrderItem 2, @CourseId = 2, @ParticipantCount = 15
go

execute dbo.P_SetOrderState 2, 'A'
go

--------------------------------------------------------------------------------

